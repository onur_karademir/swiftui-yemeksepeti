//
//  UserEntity+CoreDataProperties.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 11.09.2022.
//
//

import Foundation
import CoreData


extension UserEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserEntity> {
        return NSFetchRequest<UserEntity>(entityName: "UserEntity")
    }

    @NSManaged public var userEmail: String?
    @NSManaged public var userName: String?
    @NSManaged public var userPassword: String?
    @NSManaged public var userFav: String?

}

extension UserEntity : Identifiable {

}
