//
//  MarketTabView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 16.09.2022.
//

import SwiftUI

struct MarketTabView: View {
    @State var selectedtab : Int = 0
    @AppStorage("true_pay") var isTruePay : Bool =  false
     var body: some View {
         TabView (selection: $selectedtab){
             MerketHomePageView()
                 .tabItem {
                 Image(systemName: "house")
                     Text("Market Home")
                 }.tag(0)
             OrderFallowView(miniCardViewModel: MiniCardViewModel.init())
                 .tabItem {
                 Image(systemName: "creditcard.trianglebadge.exclamationmark")
                     Text("Order")
                 }.tag(1)
                 .badge(isTruePay ? 1 : 0)
//             InfoView()
//                 .tabItem {
//                 Image(systemName: "info")
//                     Text("Info")
//                 }.tag(2)
//                 .badge(0)
             ProfileView()
                 .tabItem {
                 Image(systemName: "person.fill")
                     Text("Profile")
                 }.tag(3)
                 .badge(0)

         }
         .onAppear {
             let appearance = UITabBarAppearance()
             appearance.backgroundEffect = UIBlurEffect(style: .systemUltraThinMaterial)
             appearance.backgroundColor = UIColor(Color.pink.opacity(0.2))
             
             // Use this appearance when scrolling behind the TabView:
             UITabBar.appearance().standardAppearance = appearance
             // Use this appearance when scrolled all the way up:
             UITabBar.appearance().scrollEdgeAppearance = appearance
         }
         .accentColor(.pink)
     }
}

struct MarketTabView_Previews: PreviewProvider {
    static var previews: some View {
        MarketTabView()
    }
}
