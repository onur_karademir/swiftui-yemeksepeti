//
//  PayPViewModel.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 2.09.2022.
//

import Foundation

struct Bookmark: Identifiable {
    let id = UUID()
    let name: String
    let icon: String
    var items: [Bookmark]?

    // some example //
    static let rest1 = Bookmark(name: "Restourant - 1", icon: "1.circle")
    static let rest2 = Bookmark(name: "Restourant - 2", icon: "square.and.pencil")
    static let rest3 = Bookmark(name: "Restourant - 3", icon: "bolt.fill")
    static let rest4 = Bookmark(name: "Restourant - 4", icon: "mic")
    
    static let menu1 = Bookmark(name: "Menu - 1", icon: "mic")
    static let menu2 = Bookmark(name: "Menu - 2", icon: "bolt.fill")
    static let menu3 = Bookmark(name: "Menu - 3", icon: "square.and.pencil")
    
    static let ord1 = Bookmark(name: "Last Ord. - 1", icon: "mic")
    static let ord2 = Bookmark(name: "Last Ord. - 2", icon: "bolt.fill")
    static let ord3 = Bookmark(name: "Last Ord. - 3", icon: "square.and.pencil")
    

    // some example groups
    static let example1 = Bookmark(name: "Favorites Restourant", icon: "star", items: [Bookmark.rest1, Bookmark.rest2, Bookmark.rest3, Bookmark.rest4])
    
    static let example2 = Bookmark(name: "Favorites Menu", icon: "timer", items: [Bookmark.menu1, Bookmark.menu2, Bookmark.menu3])
    
    static let example3 = Bookmark(name: "Last Orders", icon: "timer", items: [Bookmark.ord1, Bookmark.ord2, Bookmark.ord3])
}
