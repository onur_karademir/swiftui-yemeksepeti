//
//  PayPageView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 1.09.2022.
//

import SwiftUI
// import core data for buying list ? //
import CoreData

class CoreDataViewModel : ObservableObject  {
    // container core data //
    let container : NSPersistentContainer
    // my entity //
    @Published var saveEntities : [BuyEntity] = []
    
    init () {
        container = NSPersistentContainer(name: "BuyContainer")//<=dosya ismi olmai mutlaka !!! //
        
        container.loadPersistentStores { description, error in
            if let error = error {
                print("error loading core data ! \(error)")
            }
        }
        fatchItems()
    }
    
    func fatchItems () {
        let request = NSFetchRequest<BuyEntity>(entityName: "BuyEntity")
        do {
            saveEntities = try container.viewContext.fetch(request)
        }catch let error {
            print("error \(error)")
        }
    }
    
    func addData (rest: String, foods: String, img: String, icon: String, price: String, discount: String, minute : String, line: String, orderCount: String, visitCount: String, desc: String) {
        let newItem = BuyEntity(context: container.viewContext)
        newItem.rest = rest
        newItem.food = foods
        newItem.image = img
        newItem.icon = icon
        newItem.price = price
        newItem.disc = discount
        newItem.minute = minute
        newItem.line = line
        newItem.orderCount = orderCount
        newItem.visitCount = visitCount
        newItem.desc = desc
        saveData()
    }
    
    func saveData () {
        do {
            try container.viewContext.save()
            fatchItems()
        }catch let error{
            print("error \(error)")
        }
    }
}


struct PayPageView: View {
    //app storage
    @AppStorage("index_number") var indexNumber : Int = 0
    @AppStorage("index_data_array") var indexArray : Int = 0
    @AppStorage("selected_menu") var selectedMenu : String = ""
    @AppStorage("index_ordered_array") var indexOrderArray : Int = 0
    @AppStorage("true_pay") var isTruePay : Bool =  false
    @AppStorage("is_market") var isMarket : Bool = false
    @Environment(\.defaultMinListRowHeight) var minRowHeight
    @Environment (\.presentationMode) var presentationMode
    @State var isPresentingConfirm : Bool = false
    @State var isPresentingPayConfirm : Bool = false
    @State var isPaying : Bool = false
    // StateObject //
    @StateObject var vm = CoreDataViewModel()
    var body: some View {
        VStack (spacing: 20){
            
            PayPageHeaderView(closeButtonIcon: "xmark", headerLogo: "logo")
            
            YemekPaketiHeaderView(yemekPeketiText: isMarket ? "MarketPaketi" : "YemekPaketi", yemekPaketiLogo: "signature", yemekPaketiFont: .title, yemekPaketiSecondFont: Font.title2.weight(.bold), spacing: 20, yemekPaketiColor: .pink)
            
            if selectedMenu.count >= 1 {
                VStack {
                    VStack {
                        HStack (alignment: .center, spacing: 20) {
                            Image(isMarket ? cardMarketArray[indexArray].image : cardArray[indexArray].image)
                                .resizable()
                                .scaledToFill()
                                .frame(width: 100, height: 88)
                                .clipped()
                                .cornerRadius(10)
                                .padding(.trailing, 20)
                            VStack (alignment: .leading, spacing: 10) {
                                HStack {
                                    Text(isMarket ? cardMarketArray[indexArray].restName : cardArray[indexArray].restName)
                                    Text(isMarket ? cardMarketArray[indexArray].title : cardArray[indexArray].title)
                                }
                                .font(.headline)
                                .foregroundColor(.black)
                                Label("25 mn.", systemImage: "clock.badge.exclamationmark")
                                    .font(.subheadline)
                                    .foregroundColor(.secondary)
                            }
                        }
                    }
                    Divider()
                    
                    HStack (alignment: .center, spacing: 40){
                        Button {
                            //ode
                            isPresentingPayConfirm = true
                        } label: {
                            ButtonPayView(title: "Pay", imageSystemName: "checkerboard.shield", buttonColor: .green)
                        }
                        .confirmationDialog("Are you sure?",
                          isPresented: $isPresentingPayConfirm) {
                          Button("Pay all items?", role: .destructive) {
                              isPaying = true
                              isTruePay = isPaying
                              indexOrderArray = indexArray
                              selectedMenu = ""
                              vm.addData(
                                rest: isMarket ? cardMarketArray[indexArray].restName : cardArray[indexArray].restName,
                                
                                foods: isMarket ? cardMarketArray[indexArray].title : cardArray[indexArray].title,
                                
                                img: isMarket ? cardMarketArray[indexArray].image : cardArray[indexArray].image,
                                
                                icon: isMarket ? cardMarketArray[indexArray].icon : cardArray[indexArray].icon,
                                
                                price: isMarket ? cardMarketArray[indexArray].price : cardArray[indexArray].price,
                                
                                discount: isMarket ? cardMarketArray[indexArray].discount : cardArray[indexArray].discount,
                                
                                minute: isMarket ? cardMarketArray[indexArray].minute : cardArray[indexArray].minute,
                                
                                line: isMarket ? cardMarketArray[indexArray].line : cardArray[indexArray].line,
                                
                                orderCount: isMarket ? cardMarketArray[indexArray].orderCount : cardArray[indexArray].orderCount,
                                
                                visitCount: isMarket ? cardMarketArray[indexArray].visitCount: cardArray[indexArray].visitCount,
                                
                                desc: isMarket ? cardMarketArray[indexArray].desc : cardArray[indexArray].desc
                              )
                           }
                         }
                        Button {
                            //sil
                            isPresentingConfirm = true
                        } label: {
                            ButtonPayView(title: "Delete", imageSystemName: "trash", buttonColor: .red)
                        }
                        .confirmationDialog("Are you sure?",
                          isPresented: $isPresentingConfirm) {
                          Button("Delete all items?", role: .destructive) {
                              selectedMenu = ""
                           }
                         }
                    }
                    Divider()
                    //RegisterCardView(regCardTitle: "Registered Card", regCardImg: "card1", regCardNoTitle: "Card No:")
                    Divider()
                    PayPageListView()
                    Spacer()
                }
            }else {
                VStack {
                    if isPaying {
                        PayNotifView(isPayText: "Ordered Succes", isPayImg: "figure.wave", paySubTitle: "Go to the order page track it.")
                    } else {
                        PayPageEmptyView(emPtyText: "List Empty", emptyImg: "figure.wave", emptySubTitle: "")
                    }
                    ScrollView (showsIndicators: false) {
                        //RegisterCardView(regCardTitle: "Registered Card", regCardImg: "card1", regCardNoTitle: "Card No:")
                        Divider()
                        PayPageListView()
                        //MiniListView()
                        BestSellerTextView(bestSellerText: "Last 5 purchased menus.")
                        Divider()
                        ScrollView (.horizontal, showsIndicators: false) {
                            LazyHStack (spacing: 16) {
                                ForEach(vm.saveEntities.reversed().prefix(5)) { item in
                                    NavigationLink {
                                    } label: {
                                        MiniCardView(
                                            image: item.image ?? "",
                                            title: item.food ?? "",
                                            price: item.price ?? "",
                                            discount: item.disc ?? "",
                                            icon: item.icon ?? ""
                                        )
                                    }
                                    .simultaneousGesture(
                                        TapGesture().onEnded{
                                            presentationMode.wrappedValue.dismiss()
                                    })
                                }
                            }
                            .padding(.vertical)
                        }
                        BestSellerTextView(bestSellerText: "Kitchen & Categories")
                        Divider()
                        ScrollView (.horizontal, showsIndicators: false) {
                            HStack (spacing: 16) {
                                ForEach(isMarket ? cardMarketArray : cardArray) { item in
                                    CatogoriesView(cardData: item, Width: 70, Height: 70, IconSize: 30, fontSize: .subheadline, colorType: isMarket ? .pink : .orange)
                                }
                            }
                            .padding()
                        }

                        Spacer()
                    }

                }
            }
            
        }
        
    }
}

struct PayPageView_Previews: PreviewProvider {
    static var previews: some View {
        PayPageView()
    }
}
