//
//  ButtonOrderView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 31.08.2022.
//

import SwiftUI

struct ButtonOrderView: View {
    var title : String
    var imageSystemName : String
    var body: some View {
        HStack (alignment: .center , spacing: 19){
            Text(title)
                .bold()
            Image(systemName: imageSystemName)
        }
        .frame(width: 250, height: 50, alignment: .center)
        .font(.title)
        .background(.pink)
        .cornerRadius(10)
        .foregroundColor(.white)
    }
}

struct ButtonOrderView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonOrderView(title: "Add Basket", imageSystemName: "cart.fill.badge.plus")
    }
}
