//
//  PaymentSettingsView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 5.09.2022.
//

import SwiftUI

struct PaymentSettingsView: View {
    let sectionName : String
    @Binding var selected: Int
    //@State var selectedCurrency: Int = 0
    @Binding var currencyArray : [String]
    var body: some View {
        Picker(selection: self.$selected, label: Text(sectionName)) {
            ForEach(0 ..< self.currencyArray.count, id:\.self) {
                    Text(self.currencyArray[$0]).tag($0)
                }
        }
    }
}

struct PaymentSettingsView_Previews: PreviewProvider {
    static var previews: some View {
        PaymentSettingsView(sectionName: "Currency", selected: .constant(0), currencyArray: .constant([]))
    }
}
