//
//  MiniSearchItemView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 24.09.2022.
//

import SwiftUI

struct MiniSearchItemView: View {
    var cardData : CardModel = cardArray[2]
    var body: some View {
        HStack (spacing: 6) {
            Image(cardData.image)
                .resizable()
                .frame(width: 20, height: 20, alignment: .center)
                .clipShape(Circle())
            Text(cardData.title)
                .foregroundColor(.white)
                .font(.footnote)
                .onTapGesture {
                    print("mini card value ==>> \(cardData.title)")
                }
                //.frame(width: 40)
        }
        .frame(width: 70)
        .padding(7)
        .background(.pink)
        .clipShape(Capsule())
        .overlay(
          Image(systemName: "xmark")
              .foregroundColor(.black)
              .font(.system(size: 13, weight: .bold))
              .onTapGesture {
                  
              }
          ,alignment:.topTrailing
    )

    }
}

struct MiniSearchItemView_Previews: PreviewProvider {
    static var previews: some View {
        MiniSearchItemView()
    }
}
