//
//  ProfileHeaderView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 7.09.2022.
//

import SwiftUI

struct ProfileHeaderView: View {
    let createListHeaderImg : String
    var body: some View {
        VStack (spacing: 5){
            HStack{Spacer()}
            Image(systemName: createListHeaderImg)
                .foregroundColor(.white)
                .font(.largeTitle)
            Text("photo")
                .font(.caption2)
                .foregroundColor(.white)
        }
        .frame(height: 140, alignment: .center)
        .background(.pink).opacity(0.5)
    }
}

struct ProfileHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileHeaderView(createListHeaderImg: "camera")
    }
}
