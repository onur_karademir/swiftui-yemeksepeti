//
//  AuthenticationHeaderView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 8.09.2022.
//

import SwiftUI

struct AuthenticationHeaderView: View {
    var titleText : String
    var subtitleText : String
    var image : String
    var bgColor : Color
    
    var body: some View {
        VStack (alignment: .leading, spacing: 0) {
            //2 hstack icinde spacer
            HStack{Spacer()}
            Spacer()
            Spacer()
            Text(titleText)
                .font(.largeTitle)
                .fontWeight(.semibold)
            HStack (spacing: 13){
                Text(subtitleText)
                    .font(.largeTitle)
                    .fontWeight(.semibold)
                Image(image)
                    .resizable()
                    .frame(width: 50, height: 50)
                    .clipShape(Circle())
            }
            Spacer()
            YemekPaketiHeaderView(yemekPeketiText: "YemekPaketi", yemekPaketiLogo: "signature", yemekPaketiFont: .title3, yemekPaketiSecondFont: Font.headline.weight(.bold), spacing: 6, yemekPaketiColor: .pink)
        }
        .frame(height: 260)
        .padding(.leading)
        //ekrana tamamen yaymak icin iki pratik yol var.
        //1 frame icine uiscreen kullnamak
        //.frame(width: UIScreen.main.bounds.width, height: 260)
        .background(bgColor)
        .foregroundColor(.white)
        .cornerRadius(0)
    }
}

struct AuthenticationHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        AuthenticationHeaderView(titleText: "Hello.", subtitleText: "Welcome Back", image: "logo", bgColor: .pink)
    }
}
