//
//  PayMethodView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 15.09.2022.
//

import SwiftUI

struct PayMethodView: View {
    let payMethodText : String
    let payMethodImg : String
    let payMethodImgVisa :  String
    var body: some View {
        VStack (alignment: .leading, spacing: 7) {
            Text(payMethodText)
                .underline()
                .italic()
                .font(.footnote)
                .foregroundColor(.gray)
            HStack {
                Image(payMethodImg)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 50, height: 30)
                    .clipped()
                    .cornerRadius(10)
                Image(payMethodImgVisa)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 50, height: 30)
                    .clipped()
                    .cornerRadius(10)
            }
        }

    }
}

struct PayMethodView_Previews: PreviewProvider {
    static var previews: some View {
        PayMethodView(payMethodText: "Pay & Method", payMethodImg: "mcardv", payMethodImgVisa: "card1")
    }
}
