//
//  MenuButtonView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 30.08.2022.
//

import SwiftUI

struct MenuButtonView: View {
    let menuButtonView : String
    var body: some View {
        Image(systemName: menuButtonView)
            .resizable()
            .frame(width: 25, height: 20)
            .foregroundColor(.white)
            //.offset(x: 15, y: 15)
    }
}

struct MenuButtonView_Previews: PreviewProvider {
    static var previews: some View {
        MenuButtonView(menuButtonView: "menucard")
    }
}
