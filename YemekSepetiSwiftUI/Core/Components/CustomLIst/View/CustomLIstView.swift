//
//  CustomLIstView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 20.09.2022.
//

import SwiftUI

struct CustomLIstView: View {
    @AppStorage("is_market") var isMarket : Bool = false
    init() {
            UITableView.appearance().showsVerticalScrollIndicator = false
        }
    var body: some View {
        VStack {
            List {
                ForEach(isMarket ? cardMarketArray : cardArray, id:\.id) { item in
                    VStack {
                        HStack (alignment: .center, spacing: 10) {
                            Image(item.image)
                                .resizable()
                                .scaledToFill()
                                .frame(width: 80, height: 50, alignment: .center)
                                .clipped()
                                .cornerRadius(10)
                                .overlay(
                                    Text(item.line)
                                        .foregroundColor(.white)
                                        .font(.caption2)
                                        .fontWeight(.semibold)
                                        .padding(6)
                                        .background(item.color)
                                        .cornerRadius(20)
                                        .clipShape(Circle())
                                    ,alignment: .topTrailing
                                )
                            VStack (alignment: .leading,spacing: 4) {
                                Text(item.title)
                                    .font(.title3)
                                    .fontWeight(.semibold)
                                Label(item.orderCount, systemImage: "cart.fill.badge.plus")
                                    .font(.caption2)
                                    .foregroundColor(.secondary)
                                Label(item.visitCount, systemImage: "eye.fill")
                                    .font(.caption2)
                                    .foregroundColor(.secondary)
                            }
                            VStack {
                                Text(item.price)
                                    .strikethrough()
                                    .font(.subheadline)
                                    .fontWeight(.bold)
                                    .foregroundColor(.red)
                                Text(item.discount)
                                    .font(.headline)
                                    .fontWeight(.bold)
                                    .foregroundColor(.green)
                            }
                            Spacer()
                            VStack {
                                Circle()
                                    .fill(item.color)
                                    .frame(width: 50,height: 50)
                                    .padding(0)
                                    .overlay(
                                        VStack (alignment: .center, spacing: 0){
                                            Image(systemName: item.icon)
                                                .font(.subheadline)
                                                .foregroundColor(.white)
                                        }
                                    )
                            }
                        }
                    }
                }
            }
            .listStyle(.inset)
        }
    }
}

struct CustomLIstView_Previews: PreviewProvider {
    static var previews: some View {
        CustomLIstView()
    }
}
