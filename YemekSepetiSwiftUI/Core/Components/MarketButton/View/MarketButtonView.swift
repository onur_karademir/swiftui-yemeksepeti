//
//  MarketButtonView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 16.09.2022.
//

import SwiftUI

struct MarketButtonView: View {
    let marketButtonName : String
    let marketName : String
    var body: some View {
        VStack (spacing: 1){
            Image(systemName: marketButtonName)
                .resizable()
                .renderingMode(.template)
                .frame(width: 25, height: 25)
            Text(marketName)
                .font(.caption2)
                .foregroundColor(.white)
            
        }
        .foregroundColor(.white)
    }
}

struct MarketButtonView_Previews: PreviewProvider {
    static var previews: some View {
        MarketButtonView(marketButtonName: "highlighter", marketName: "Market")
    }
}
