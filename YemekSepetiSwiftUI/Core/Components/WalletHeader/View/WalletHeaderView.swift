//
//  WalletHeaderView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 13.09.2022.
//

import SwiftUI

struct WalletHeaderView: View {
    let walletText : String
    var body: some View {
        VStack {
            HStack{Spacer()}
            HStack (spacing: 9) {
                Image(systemName: "menucard")
                    .font(.title2)
                    .foregroundColor(.white)
                Text(walletText)
                    .font(.title)
                    .foregroundColor(.white)
                    .italic()
                Spacer()
            }
            .padding(.leading)
        }
        .padding()
        .background(Color.pink)
        .border(Color.gray, width: 7)
    }
}

struct WalletHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        WalletHeaderView(walletText: "Wallet...")
    }
}
