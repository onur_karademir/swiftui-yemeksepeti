//
//  PayNotifView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 3.09.2022.
//

import SwiftUI

struct PayNotifView: View {
    let isPayText : String
    let isPayImg : String
    let paySubTitle : String
    var body: some View {
        VStack {
            VStack {
                HStack{Spacer()}.padding(0)
                HStack {
                    Text(isPayText)
                        .font(.title)
                        .fontWeight(.bold)
                        .padding(0)
                    Image(systemName: isPayImg)
                        .font(Font.title.weight(.bold))
                        .padding(0)
                }
                .foregroundColor(.white)
                
                Text(paySubTitle)
                    .foregroundColor(.white)
            }
            .padding()
            .background(Color.green)
        }
    }
}

struct PayNotifView_Previews: PreviewProvider {
    static var previews: some View {
        PayNotifView(isPayText: "Ordered Succes", isPayImg: "figure.wave", paySubTitle: "Go to the order page track it.")
    }
}
