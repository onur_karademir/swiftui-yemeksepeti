//
//  PayPageEmptyView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 2.09.2022.
//

import SwiftUI

struct PayPageEmptyView: View {
    let emPtyText : String
    let emptyImg : String
    let emptySubTitle : String
    var body: some View {
        VStack {
            VStack {
                HStack{Spacer()}.padding(0)
                HStack {
                    Text(emPtyText)
                        .font(.title)
                        .fontWeight(.bold)
                        .padding(0)
                    Image(systemName: emptyImg)
                        .font(Font.title.weight(.bold))
                        .padding(0)
                }
                .foregroundColor(.white)
                Text(emptySubTitle)
                    .foregroundColor(.white)
            }
            .padding()
            .background(Color.cyan)
        }
    }
}

struct PayPageEmptyView_Previews: PreviewProvider {
    static var previews: some View {
        PayPageEmptyView(emPtyText: "Listeniz Bos", emptyImg: "nosign", emptySubTitle: "")
    }
}
