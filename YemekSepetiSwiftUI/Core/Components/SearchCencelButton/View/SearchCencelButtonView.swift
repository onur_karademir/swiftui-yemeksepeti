//
//  SearchCencelButtonView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 22.09.2022.
//

import SwiftUI

struct SearchCencelButtonView: View {
    let cancelButtonText : String
    let cancelButtonColor : Color
    var body: some View {
        Text(cancelButtonText)
            .font(.headline)
            .fontWeight(.semibold)
            .padding(9)
            .foregroundColor(.white)
            .background(cancelButtonColor)
            .clipShape(Capsule())
    }
}

struct SearchCencelButtonView_Previews: PreviewProvider {
    static var previews: some View {
        SearchCencelButtonView(cancelButtonText: "Cancel", cancelButtonColor: .pink)
    }
}
