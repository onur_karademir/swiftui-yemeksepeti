//
//  CaroselViewModel.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 31.08.2022.
//

import Foundation
import SwiftUI

struct CaroselModel : Identifiable {
    var id = UUID()
    var bannerTitle : String
    var bannerBgColor : Color
}


var caroselData = [
    CaroselModel(bannerTitle: "Banner 1", bannerBgColor: .cyan),
    CaroselModel(bannerTitle: "Banner 2", bannerBgColor: .indigo),
    CaroselModel(bannerTitle: "Banner 3", bannerBgColor: .mint),
    CaroselModel(bannerTitle: "Banner 4", bannerBgColor: .purple)
]

var marketCaroselData = [
    CaroselModel(bannerTitle: "Market Banner 1", bannerBgColor: .purple),
    CaroselModel(bannerTitle: "Market Banner 2", bannerBgColor: .mint),
    CaroselModel(bannerTitle: "Market Banner 3", bannerBgColor: .indigo),
    CaroselModel(bannerTitle: "Market Banner 4", bannerBgColor: .cyan)
]
