//
//  CaroselView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 30.08.2022.
//

import SwiftUI

struct CaroselView: View {
    var cardData : CaroselModel = caroselData[0]
    var body: some View {
        VStack (alignment: .leading, spacing: 0){
            //eski hali
//            Image(cardData.image)
//                .resizable()
//                .scaledToFill()
//                .frame(height: 190)
//                .clipped()
            
            // yeni hali
            
            Text(cardData.bannerTitle)
                .foregroundColor(.white)
                .font(.largeTitle)
                .fontWeight(.semibold)
                .underline()
                .frame(
                  minWidth: 0,
                  maxWidth: .infinity,
                  minHeight: 0,
                  maxHeight: .infinity,
                  alignment: .center
                )
                
        }
        //.cornerRadius(20)
        .background(cardData.bannerBgColor)
        .padding(0)
    }
}

struct CaroselView_Previews: PreviewProvider {
    static var previews: some View {
        CaroselView()
    }
}
