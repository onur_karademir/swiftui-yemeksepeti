//
//  RegisterCardDetailView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 17.09.2022.
//

import SwiftUI

struct RegisterCardDetailView: View {
    // app storage //
    @AppStorage("pm_point") var currentUserPm : Int = 0
    @AppStorage("user_name") var currentUserName: String?
    @State var selection = "Card - 1"
    @Binding var isSelectedCard : Bool
    @State var bgColor = Color.pink
    let cards = ["Card - 1", "Card - 2"]
    var body: some View {
        VStack {
            PayPageHeaderView(closeButtonIcon: "xmark", headerLogo: "logo")
            VStack {
                Image(isSelectedCard ? "card2" : "card1")
                    .resizable()
                    .frame(maxWidth:.infinity, maxHeight: 350)
                    .aspectRatio(contentMode: .fit)
                    .scaledToFit()
                    .clipped()
                    .cornerRadius(20)
            }
            ScrollView {
                
                VStack (spacing: 10) {
                    
                    VStack (alignment: .leading) {
                        HStack{Spacer()}
                        VStack (alignment: .leading, spacing: 16) {
                            HStack {
                                Text("Name:")
                                    .font(.title2)
                                    .foregroundColor(.black)
                                    .fontWeight(.semibold)
                                Text("UserName")
                                    .font(.title3)
                                    .foregroundColor(.black)
                            }
                            HStack {
                                Text("Last Name:")
                                    .font(.title2)
                                    .foregroundColor(.black)
                                    .fontWeight(.semibold)
                                Text("UserLastName")
                                    .font(.title3)
                                    .foregroundColor(.black)
                            }
                            HStack {
                                Text("Card Type:")
                                    .font(.title2)
                                    .foregroundColor(.black)
                                    .fontWeight(.semibold)
                                Text(isSelectedCard ? "Visa Card" : "Master Card")
                                    .font(.title3)
                                    .foregroundColor(.black)
                            }
                            
                            HStack {
                                Text("Card No:")
                                    .font(.title2)
                                    .foregroundColor(.black)
                                    .fontWeight(.semibold)
                                Text(isSelectedCard ? "99xx - xxxx - xxxx - xx54" : " 43xx - xxxx - xxxx - xx89 ")
                                    .font(.callout)
                                    .foregroundColor(.black)
                            }
                            
                            HStack {
                                Text("Card: ")
                                    .font(.title2)
                                    .foregroundColor(.black)
                                    .fontWeight(.semibold)
                                Text("\(selection)")
                                    .font(.callout)
                                    .foregroundColor(.black)
                            }
                        }
                        
                    }
                    .padding()
                    .background(bgColor).opacity(0.4)
                    
                    VStack {
                        Menu {
                            Picker("Select a card.", selection: $selection) {
                                ForEach(cards, id: \.self) {
                                    Text($0)
                                 }
                                }
                                .pickerStyle(.menu)
                                .onChange(of: selection) { tag in
                                    if selection == "Card - 1" {
                                        isSelectedCard = false
                                    }else {
                                        isSelectedCard = true
                                    }
                                    if isSelectedCard {
                                        withAnimation(.easeInOut(duration: 1)) {
                                            bgColor = Color.green
                                        }
                                    } else {
                                        withAnimation(.easeInOut(duration: 1)) {
                                            bgColor = Color.pink
                                        }
                                    }
                                }
                        }label: {
                            Text("Select Card")
                                .foregroundColor(.white)
                                .font(.callout)
                                .padding(10)
                                .background(.pink)
                                .clipShape(Capsule())
                        }
                    }
                    .onAppear {
                        if isSelectedCard {
                            selection = "Card - 2"
                                bgColor = Color.green
                        } else {
                                bgColor = Color.pink
                        }
                    }

                    
                    VStack (alignment:.leading, spacing: 16) {
                        HStack{Spacer()}
                        
                        HStack {
                            Text("Wallet Adress: ")
                                .font(.title3)
                                .foregroundColor(.black)
                                .fontWeight(.semibold)
                            Text(currentUserName ?? "user")
                                .font(.callout)
                                .foregroundColor(.black)
                        }
                        
                        HStack {
                            Text("Wallet No: ")
                                .font(.title2)
                                .foregroundColor(.black)
                                .fontWeight(.semibold)
                            Text(" 75xx - xxxx - xxxx - xx99 ")
                                .font(.callout)
                                .foregroundColor(.black)
                        }
                        
                        HStack {
                            Text("PaketMoney: ")
                                .font(.title2)
                                .foregroundColor(.black)
                                .fontWeight(.semibold)
                            Text("\(currentUserPm)")
                                .font(.title3)
                                .foregroundColor(.black)
                        }
                    }
                    .padding()
                    .background(Color(red: 242 / 255, green: 242 / 255, blue: 242 / 255))
                                        
                }
            }
            Spacer()
        }
        .padding(.horizontal)
    }
}

struct RegisterCardDetailView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterCardDetailView(isSelectedCard: .constant(false))
    }
}
