//
//  InfoCardView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 10.09.2022.
//

import SwiftUI

struct InfoCardView: View {
    let infoCardImg : String
    let infoCardTitle : String
    let infoCardSubtitle : String
    var body: some View {
        VStack {
            HStack (alignment: .center) {
                Circle()
                    .fill(Color.pink)
                    .frame(width: 90, height: 90)
                    .overlay(
                        Image(systemName: infoCardImg)
                            .foregroundColor(.white)
                            .font(.largeTitle)
                    )
                VStack (alignment: .leading, spacing: 10) {
                    Text(infoCardTitle)
                        .fontWeight(.semibold)
                        .underline()
                        .font(.title2)
                    Text(infoCardSubtitle)
                        .font(.title3)
                }
            }
        }
        .padding(20)
        .background(Color(.systemGray6))
        .cornerRadius(10)

    }
}

struct InfoCardView_Previews: PreviewProvider {
    static var previews: some View {
        InfoCardView(infoCardImg: "person", infoCardTitle: "Project Owner", infoCardSubtitle: "ONUR KARADEMIR")
    }
}
