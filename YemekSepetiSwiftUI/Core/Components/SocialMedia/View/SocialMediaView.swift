//
//  SocialMediaView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 15.09.2022.
//

import SwiftUI

struct SocialMediaView: View {
    let socialMediaText : String
    var body: some View {
        VStack (alignment: .leading, spacing: 7){
            Text(socialMediaText)
                .underline()
                .italic()
                .font(.footnote)
                .foregroundColor(.gray)
            HStack {
                ForEach(socialMediaData,id:\.id) { item in
                    Circle()
                        .foregroundColor(item.socialColor)
                        .frame(width: 20, height: 20, alignment: .center)
                }
            }
        }
    }
}

struct SocialMediaView_Previews: PreviewProvider {
    static var previews: some View {
        SocialMediaView(socialMediaText: "Social Media")
    }
}
