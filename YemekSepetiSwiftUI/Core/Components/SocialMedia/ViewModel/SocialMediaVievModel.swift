//
//  SocialMediaVievModel.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 15.09.2022.
//

import Foundation
import SwiftUI

struct SocialMediaDataModel : Identifiable {
    let id = UUID()
    let socialColor : Color
}

let socialMediaData = [
    SocialMediaDataModel(socialColor: .pink),
    SocialMediaDataModel(socialColor: .purple),
    SocialMediaDataModel(socialColor: .yellow),
    SocialMediaDataModel(socialColor: .blue)
]
