//
//  PayPageHeaderView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 2.09.2022.
//

import SwiftUI

struct PayPageHeaderView: View {
    @Environment (\.presentationMode) var presentationMode
    let closeButtonIcon : String
    let headerLogo : String
    var body: some View {
        VStack (alignment: .leading) {
            HStack {
                Image(headerLogo)
                    .resizable()
                    .frame(width: 45, height: 45)
                    .clipShape(Circle())
                Spacer()
                VStack {
                    Button {
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        Image(systemName: closeButtonIcon)
                            .padding()
                            .foregroundColor(Color.black)
                            .font(Font.subheadline.weight(.bold))
                    }
                    
                }
            }
        }
        .padding(.horizontal)
        .padding(.top)
    }
}

struct PayPageHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        PayPageHeaderView(closeButtonIcon: "xmark", headerLogo: "logo")
    }
}
