//
//  LikeCardView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 12.09.2022.
//

import SwiftUI

struct LikeCardView: View {
    let likeImage : String
    let likeRest : String
    let likeFood : String
    @ObservedObject var vm : CoreDataUserViewModel
    var body: some View {
        VStack {
            HStack (alignment: .top) {
                Image(likeImage)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 150, height: 150)
                    .clipped()
                    .cornerRadius(20)
                
                VStack (alignment: .leading, spacing: 10) {
                    Text("Restourant name:")
                        .font(.subheadline)
                        .foregroundColor(.gray)
                    Text(likeRest)
                        .font(.title3)
                        .fontWeight(.semibold)
                    Text("Food name:")
                        .font(.subheadline)
                        .foregroundColor(.gray)
                    Text(likeFood)
                        .font(.subheadline)
                }
                .padding(.vertical)
                .padding(.leading)
                Spacer()
                VStack (spacing: 20){
                    Image(systemName: "heart.fill")
                        .font(.largeTitle)
                        .foregroundColor(.red)
                    Button {
                        // delete manuel //
                        //vm.saveLikes.remove(at:vm.saveLikes.firstIndex(of: item)!)
                        
                        // delete vire model fuction //
                        //vm.deleteLikes(item: item)
                    } label: {
                        Text("del")
                    }

                }
                .padding(.vertical)
            }
        }
        .frame(minWidth: 0,maxWidth: .infinity,minHeight: 0,maxHeight: .infinity,alignment: .leading)
        .padding(6)
        .background(Color(.systemGray6))
        .cornerRadius(20)

    }
}

struct LikeCardView_Previews: PreviewProvider {
    static var previews: some View {
        LikeCardView(likeImage: "food1", likeRest: "rest", likeFood: "food", vm: CoreDataUserViewModel.init())
    }
}
