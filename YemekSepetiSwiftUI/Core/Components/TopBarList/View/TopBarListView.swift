//
//  TopBarListView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 26.09.2022.
//

import SwiftUI

struct TopBarListView: View {
    var cardData : CardModel = cardArray[3]
    var body: some View {
        HStack (alignment: .center) {
            
            Image(cardData.image)
                .resizable()
                .frame(width: 90, height: 55, alignment: .center)
                .clipShape(Capsule())
            Spacer()
            VStack (alignment: .leading, spacing: 6) {
                Text(cardData.title)
                    .font(.subheadline)
                    .fontWeight(.semibold)
                Text(cardData.restName)
                    .font(.subheadline)
                    .fontWeight(.semibold)
                    .lineLimit(1)
                
                HStack {
                    
                    HStack {
                        Image(systemName: "cart.fill.badge.plus")
                            .font(.caption2)
                            .foregroundColor(.gray)
                        Text(cardData.visitCount)
                            .font(.caption2)
                            .foregroundColor(.gray)
                    }
                    
                    HStack {
                        Image(systemName: "cart.fill.badge.plus")
                            .font(.caption2)
                            .foregroundColor(.gray)
                        Text(cardData.visitCount)
                            .font(.caption2)
                            .foregroundColor(.gray)
                    }
                    
                }
            }
            Spacer()
            VStack (spacing: 2) {
                ForEach( cardData.allFoods,id:\.self ) { item in
                    Text(item)
                        .font(.caption)
                        .foregroundColor(.white)
                        .padding(4)
                        .background(cardData.color)
                        .clipShape(Capsule())
                }
            }
            Spacer()
            VStack {
                Image(systemName: cardData.icon)
                    .font(.title3)
                    .foregroundColor(.white)
                    .padding(9)
                    .background(cardData.color)
                    .clipShape(Circle())
                
                Text(cardData.price)
                    .strikethrough()
                    .foregroundColor(.red)
                    .font(.caption2)
                    .fontWeight(.bold)
                Text(cardData.discount)
                    .foregroundColor(.green)
                    .font(.caption)
                    .fontWeight(.bold)
            }
            
        }
        .frame(maxWidth: .infinity, alignment: .leading) // width 100 //
        .padding(.vertical)
        .padding(.horizontal, 2)
        .background(Color(.systemGray4))
        .cornerRadius(20)
    }
}

struct TopBarListView_Previews: PreviewProvider {
    static var previews: some View {
        TopBarListView()
    }
}
