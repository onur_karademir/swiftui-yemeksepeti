//
//  RegisterCardView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 2.09.2022.
//

import SwiftUI

struct RegisterCardView: View {
    let regCardTitle : String
    let regCardImg : String
    let regCardNoTitle : String
    @State var isCardDetail : Bool = false
    @State var isSelectedCard : Bool = false
    var body: some View {
        HStack (alignment: .center) {
            VStack (spacing: 10) {
                Text(regCardTitle)
                    .font(.title2)
                    .fontWeight(.ultraLight)
                Image(isSelectedCard ? "card2" : regCardImg)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 130)
            }
            VStack (spacing: 7) {
                Text(regCardNoTitle)
                    .font(.footnote)
                    .fontWeight(.ultraLight)
                    .underline()
                Text(isSelectedCard ? " 99xx - xxxx - xxxx - xx54 " : " 43xx - xxxx - xxxx - xx89 ")
                    .foregroundColor(isSelectedCard ? .green : .pink)
                    .font(.callout)
                    .fontWeight(.semibold)
                    .underline()
                HStack {
                    Text("Card Type:")
                        .font(.footnote)
                        .fontWeight(.ultraLight)
                        .underline()
                    Text(isSelectedCard ? "Visa Card" : "Master Card")
                        .font(.footnote)
                        .foregroundColor(isSelectedCard ? .green : .pink)
                        .fontWeight(.semibold)
                        .underline()
                }
                Text("Detail")
                    .font(.footnote)
                    .fontWeight(.ultraLight)
                    .underline()
                Image(systemName: "cursorarrow.click.2")
                    .foregroundColor(.gray)
                    .font(.footnote)
            }
        }
        .onTapGesture {
            isCardDetail = true
        }
        .sheet(isPresented: $isCardDetail) {
            RegisterCardDetailView(isSelectedCard: $isSelectedCard)
        }
    }
}

struct RegisterCardView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterCardView(regCardTitle: "Registered Card", regCardImg: "card1", regCardNoTitle: "Card No:")
    }
}
