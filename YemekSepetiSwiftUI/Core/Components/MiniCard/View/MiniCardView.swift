//
//  MiniCardView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 4.09.2022.
//

import SwiftUI

struct MiniCardView: View {
    
    @StateObject var miniCardViewModel : MiniCardViewModel = MiniCardViewModel()
    
    let image : String
    let title : String
    let price : String
    let discount : String
    let icon : String
    
    var body: some View {
        VStack {
            
            VStack {
                HStack (alignment: .top, spacing: 10) {
                    Image(image)
                        .resizable()
                        .scaledToFill()
                        .frame(width: 90, height: 90)
                        .clipped()
                        .cornerRadius(10)
                    VStack (alignment: .center, spacing: 13) {
                        Text(title)
                            .font(.headline)
                            .foregroundColor(.black)
                            .fontWeight(.bold)
                            .lineLimit(1)
                        Text(price)
                            .strikethrough()
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .foregroundColor(.red)
                        Text(discount)
                            .font(.headline)
                            .fontWeight(.bold)
                            .foregroundColor(.green)
                    }
                }
                .frame(width: 200, height: 90, alignment: .topLeading)
                .padding(5)
                .background(Color(red: 242 / 255, green: 242 / 255, blue: 242 / 255))
                .cornerRadius(20)
                //.shadow(radius: 20)
                .overlay (
                    Circle()
                        .fill(Color.cyan)
                        .frame(width: 30,height: 30)
                        .padding(0)
                        .overlay(
                            VStack (alignment: .center, spacing: 0){
                                Image(systemName: icon)
                                    .font(.caption)
                                    .foregroundColor(.white)
                            }
                        )
                        .offset(x: 10, y: 0)
                   
                    ,alignment: .topTrailing
                )
            }
        }
    }
}

struct MiniCardView_Previews: PreviewProvider {
    static var previews: some View {
        MiniCardView(image: "food1", title: "title", price: "33", discount: "33", icon: "house")
    }
}
