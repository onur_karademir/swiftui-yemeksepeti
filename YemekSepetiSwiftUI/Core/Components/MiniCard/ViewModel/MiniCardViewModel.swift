//
//  MiniCardViewModel.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 4.09.2022.
//

import Foundation

struct MiniCardModel : Identifiable {
    let id = UUID()
    let image : String
    let title : String
    let price : String
    let discount : String
    let icon : String
}

class MiniCardViewModel : ObservableObject {
    
    @Published var miniCardViewModel : [MiniCardModel] = []
    
    init () {
        getItems()
    }
    
    func getItems(){
        
        let item1 = MiniCardModel(image: "food1", title: "food1", price: "44.99 TL", discount: "29.99 TL", icon: "house")
        
        let item2 = MiniCardModel(image: "food2", title: "food2", price: "39.99 TL", discount: "19.99 TL", icon: "bolt.heart")
        
        let item3 = MiniCardModel(image: "food3", title: "food3", price: "54.99 TL", discount: "24.99 TL", icon: "airplane.departure")
        
        let item4 = MiniCardModel(image: "food4", title: "food4", price: "59.99 TL", discount: "49.99 TL", icon: "play.slash.fill")
        
        miniCardViewModel.append(item1)
        
        miniCardViewModel.append(item2)
        
        miniCardViewModel.append(item3)
        
        miniCardViewModel.append(item4)
    }
}
