//
//  CustomTextAreaView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 30.08.2022.
//

import SwiftUI

struct CustomTextAreaView: View {
    @State var showingPopup = false
    let searchPlaceHolder : String
    let searchIcon : String
    var body: some View {
        ZStack {
            HStack (spacing: 10){
                Text(searchPlaceHolder)
                    .font(.title3)
                    .foregroundColor(.black)
                    .fontWeight(.semibold)
                    .opacity(1)
                Image(systemName: searchIcon)
                    .font(.body)
                HStack{Spacer()}
            }
            .padding(3)
            .padding(.leading)
            .background(.white)
            .cornerRadius(13)
            .opacity(0.8)
            .onTapGesture {
                showingPopup = true
            }
            .fullScreenCover(isPresented: $showingPopup) {
                SearchFilterPageView()
            }
      }
    }
}

struct CustomTextAreaView_Previews: PreviewProvider {
    static var previews: some View {
        CustomTextAreaView(searchPlaceHolder: "Serach", searchIcon: "magnifyingglass.circle")
    }
}


struct BackgroundBlurView: UIViewRepresentable {
    func makeUIView(context: Context) -> UIView {
        let view = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        DispatchQueue.main.async {
            view.superview?.superview?.backgroundColor = .clear
        }
        return view
    }

    func updateUIView(_ uiView: UIView, context: Context) {}
}

struct Blur: UIViewRepresentable {
    var style: UIBlurEffect.Style = .systemMaterial

    func makeUIView(context: Context) -> UIVisualEffectView {
        return UIVisualEffectView(effect: UIBlurEffect(style: style))
    }

    func updateUIView(_ uiView: UIVisualEffectView, context: Context) {
        uiView.effect = UIBlurEffect(style: style)
    }
}
