//
//  ProfileTextAreaView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 7.09.2022.
//

import SwiftUI

struct ProfileTextAreaView: View {
    var createListKey : String
    var createListValue : String
    var body: some View {
        VStack {
            Divider()
            HStack {
                VStack (alignment: .leading, spacing: 15){
                    Text(createListKey)
                        .font(.headline)
                        .fontWeight(.bold)
                        .foregroundColor(.black)
                    
                    Text(createListValue)
                        .font(.headline)
                        .fontWeight(.semibold)
                        .foregroundColor(.gray)
                }
                .padding(.horizontal)
                .padding(.vertical, 4)
                Spacer()
            }
            Divider()
        }
    }
}

struct ProfileTextAreaView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileTextAreaView(createListKey: "key", createListValue: "value")
    }
}
