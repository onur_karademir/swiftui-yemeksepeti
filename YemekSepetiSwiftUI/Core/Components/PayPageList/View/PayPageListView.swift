//
//  PayPageListView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 2.09.2022.
//

import SwiftUI

struct PayPageListView: View {
    let items: [Bookmark] = [.example1, .example2, .example3]
    var body: some View {
        List(items, children: \.items) { row in
            Image(systemName: row.icon)
            Text(row.name)
        }
        .listStyle(.inset)
    }
}

struct PayPageListView_Previews: PreviewProvider {
    static var previews: some View {
        PayPageListView()
    }
}
