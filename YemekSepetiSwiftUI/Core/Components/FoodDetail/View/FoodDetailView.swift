//
//  FoodDetailView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 31.08.2022.
//

import SwiftUI

struct FoodDetailView: View {
    var cardData : CardModel = cardArray[0]
    @Environment (\.presentationMode) var presentationMode
    @State var userMenu: String = ""
    @State var showActionSheet: Bool = false
    @State var showingOptions = false
    @State var selection = ""
    @State var isLikeFood : Bool = false
    // app storage //
    @AppStorage("index_number") var indexNumber : Int = 0
    @AppStorage("index_data_array") var indexArray : Int = 0
    @AppStorage("selected_menu") var selectedMenu : String = ""
    // stateObject
    @StateObject var vm : CoreDataUserViewModel = CoreDataUserViewModel()
    var body: some View {
        VStack {
            CustomHeaderSet(headerSmallText: "Food Detail", headerLogo: "logo", isButtonVisible: true)
            .padding(.horizontal)
          
            ScrollView {
                
                VStack (spacing: 10){
                    
                    Text(cardData.restName)
                        .font(.title2)
                        .fontWeight(.semibold)
                        .lineLimit(12)
                        .multilineTextAlignment(.center)
                        .padding(.horizontal)
                    
                    Image(cardData.image)
                        .resizable()
                        .scaledToFit()
                        .frame(height: 170)
                        .cornerRadius(18)
                    
                    Text(cardData.title)
                        .font(.title)
                        .fontWeight(.semibold)
                        .lineLimit(12)
                        .multilineTextAlignment(.center)
                        .padding(.horizontal)
                    
                    HStack (spacing: 40){
                        Label(cardData.visitCount, systemImage: "eye.fill")
                            .font(.subheadline)
                            .foregroundColor(.secondary)
                        Label(cardData.orderCount, systemImage: "cart.fill.badge.plus")
                            .font(.subheadline)
                            .foregroundColor(.secondary)
                        Button {
                            isLikeFood.toggle()
                            if isLikeFood {
                                vm.addLikes(
                                    like: Likes(
                                        food: cardData.title,
                                        rest: cardData.restName,
                                        image: cardData.image
                                    ))
                            }
                            print("save like array == \(vm.saveLikes)")
                        } label: {
                            Image(systemName: isLikeFood ? "heart.fill" : "heart")
                                .foregroundColor(isLikeFood ? .pink : .black)
                        }
                        .disabled(isLikeFood ? true : false)
                        .opacity(isLikeFood ? 0.8 : 1)

                    }
                    Text(cardData.desc)
                        .fontWeight(.semibold)
                        .padding(.top)
                        .padding(.bottom, 0)
                        .padding(.horizontal)
                    // select menu start//
                    VStack {
                        Text(selection.count >= 1 ? selection : "Menu not selected.")
                        Button {
                            showingOptions = true
                        } label: {
                            HStack (spacing: 4){
                                Text("Select Menu")
                                Image(systemName: "plus")
                            }
                            .font(.footnote)
                            .padding(6)
                            .foregroundColor(.white)
                            .background(.pink)
                            .clipShape(Capsule())
                        }
                        .confirmationDialog("Select a menu", isPresented: $showingOptions, titleVisibility: .visible) {
                            
                            ForEach(cardData.allFoods, id: \.self) { item in
                                Button(item) {
                                    selection = item
                                    //get selected index number //
                                    indexNumber = cardData.allFoods.firstIndex(where: {$0 == item}) ?? 0
                                }
                            }
                        }
                        .onChange(of: showingOptions) { flag in
                            if !flag {
                                if selection.count >= 1 {
                                    selection = "Your choice \(selection)"
                                    //selectedMenu = selection
                                }
                            }
                        }
                    }
                    // select menu end //
                    Button {
                        if selection.count >= 1 {
                            //selection = "Your choice \(selection)"
                            selectedMenu = selection
                        }
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        ButtonOrderView(title: "Add Basket", imageSystemName: "cart.fill.badge.plus")
                    }
                    .disabled(selection.count >= 1 ? false : true)
                    .opacity(selection.count >= 1 ? 1 : 0.5)
                }
            }
        }
    }
}

struct FoodDetailView_Previews: PreviewProvider {
    static var previews: some View {
        FoodDetailView()
    }
}
