//
//  SearchTitleView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 23.09.2022.
//

import SwiftUI

struct SearchTitleView: View {
    let titleString : String
    var body: some View {
        Text(titleString)
            .underline()
            .font(.title2)
            .fontWeight(.semibold)
            .foregroundColor(.pink)
            .italic()
            .multilineTextAlignment(.leading)
    }
}

struct SearchTitleView_Previews: PreviewProvider {
    static var previews: some View {
        SearchTitleView(titleString: "Search Tags")
    }
}
