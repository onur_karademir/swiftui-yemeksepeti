//
//  SearchItemView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 24.09.2022.
//

import SwiftUI

struct SearchItemView: View {
    var cardData : CardModel = cardArray[2]
    var body: some View {
        VStack (alignment: .leading) {
            HStack{Spacer()}
            HStack (alignment: .top) {
                Image(cardData.image)
                    .resizable()
                    .frame(width: 50, height: 50, alignment: .center)
                    .clipShape(Circle())
                
                VStack (alignment: .leading, spacing: 6) {
                    Text(cardData.title)
                        .font(.subheadline)
                        .fontWeight(.semibold)
                    HStack (spacing: 0){
                        Image(systemName: "eye.fill")
                            .font(.caption2)
                            .foregroundColor(.gray)
                        Text(cardData.orderCount)
                            .font(.caption2)
                            .foregroundColor(.gray)
                    }
                }
                VStack (alignment:.leading, spacing: 6) {
                    Text(cardData.restName)
                        .font(.subheadline)
                        .fontWeight(.semibold)
                    HStack {
                        Image(systemName: "cart.fill.badge.plus")
                            .font(.caption2)
                            .foregroundColor(.gray)
                        Text(cardData.visitCount)
                            .font(.caption2)
                            .foregroundColor(.gray)
                    }
                }
                Spacer()
                Image(systemName: "chevron.right")
                    .foregroundColor(.gray)
                    .font(.subheadline)
                    .padding()
            }
            .foregroundColor(.black)
            //Divider()
        }
        .padding(.horizontal)
        .padding(.vertical,5)
        .background(.white)
    }
}

struct SearchItemView_Previews: PreviewProvider {
    static var previews: some View {
        SearchItemView()
    }
}
