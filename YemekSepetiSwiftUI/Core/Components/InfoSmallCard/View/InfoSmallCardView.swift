//
//  InfoSmallCardView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 10.09.2022.
//

import SwiftUI

struct InfoSmallCardView: View {
    let infoSmallCardText : String
    var body: some View {
        VStack (alignment: .leading, spacing: 10) {
            Text(infoSmallCardText)
                .font(.subheadline)
                .lineSpacing(8)
                .frame(width: 280, alignment: .leading)
            
            Link("Gitlab Code Click Here", destination: URL(string: "https://gitlab.com/onur_karademir/swiftui-yemeksepeti")!)
            
        }
        .padding(15)
        .background(Color(.systemGray6))
        .cornerRadius(10)

    }
}

struct InfoSmallCardView_Previews: PreviewProvider {
    static var previews: some View {
        InfoSmallCardView(infoSmallCardText: "This application is made for educational purposes. it has no commercial purpose. Thanks.")
    }
}
