//
//  YemekPaketiHeaderView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 2.09.2022.
//

import SwiftUI

struct YemekPaketiHeaderView: View {
    let yemekPeketiText : String
    let yemekPaketiLogo : String
    let yemekPaketiFont : Font
    let yemekPaketiSecondFont : Font
    let spacing : CGFloat
    let yemekPaketiColor : Color
    var body: some View {
        VStack {
            HStack{Spacer()}.padding(0)
            HStack (spacing: spacing){
                Text(yemekPeketiText)
                    .font(yemekPaketiFont)
                    .fontWeight(.bold)
                    .italic()
                    .padding(0)
                Image(systemName: yemekPaketiLogo)
                    .font(yemekPaketiSecondFont)
                    .padding(0)
            }
        }
        .padding(10)
        .background(yemekPaketiColor)
        .foregroundColor(.white)
        .cornerRadius(20)
        .padding(.horizontal, 9)
    }
}

struct YemekPaketiHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        YemekPaketiHeaderView(yemekPeketiText: "YemekPaketi", yemekPaketiLogo: "signature", yemekPaketiFont: .title, yemekPaketiSecondFont: Font.title2.weight(.bold), spacing: 20, yemekPaketiColor: .pink)
    }
}
