//
//  MiniListView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 2.09.2022.
//

import SwiftUI

struct MiniListView: View {
    var body: some View {
        List {
            ForEach(cardArray, id:\.id) { item in
                HStack (alignment: .top, spacing: 20) {
                    Image(item.image)
                        .resizable()
                        .scaledToFill()
                        .frame(width: 88)
                        .clipped()
                        .cornerRadius(10)
                    VStack (alignment: .leading, spacing: 10) {
                        HStack {
                            Text(item.restName)
                            Text(item.title)
                        }
                        .font(.headline)
                        .foregroundColor(.black)
                        Label(item.visitCount, systemImage: "eye.fill")
                            .font(.subheadline)
                            .foregroundColor(.secondary)
                    }
                }
                .padding(5)
            }
        }
        .listStyle(.inset)
    }
}

struct MiniListView_Previews: PreviewProvider {
    static var previews: some View {
        MiniListView()
    }
}
