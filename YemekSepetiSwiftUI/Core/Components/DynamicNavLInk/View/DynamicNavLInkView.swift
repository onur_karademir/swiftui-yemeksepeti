//
//  DynamicNavLInkView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 5.09.2022.
//

import SwiftUI

struct DynamicNavLInkView<Content: View>: View {
    var content: () -> Content
    var navText : String = "Profile Information"
    var body: some View {
        VStack {
            NavigationLink(destination: content()) {
                 Text(navText)
            }
        }
    }
}

//struct DynamicNavLInkView_Previews: PreviewProvider {
//    static var previews: some View {
//        DynamicNavLInkView()
//    }
//}
