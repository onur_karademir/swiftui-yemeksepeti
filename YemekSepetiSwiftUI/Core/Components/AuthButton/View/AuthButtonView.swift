//
//  AuthButtonView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 8.09.2022.
//

import SwiftUI

struct AuthButtonView: View {
    let buttonString : String
    var body: some View {
        Text(buttonString)
            .foregroundColor(.white)
            .frame(width: 340, height: 50)
            .background(.pink)
            .clipShape(Rectangle())
            .padding()
    }
}

struct AuthButtonView_Previews: PreviewProvider {
    static var previews: some View {
        AuthButtonView(buttonString: "Sign In")
    }
}
