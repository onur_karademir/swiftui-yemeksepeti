//
//  ButtonPayView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 2.09.2022.
//

import SwiftUI

struct ButtonPayView: View {
    var title : String
    var imageSystemName : String
    var buttonColor : Color
    var body: some View {
        HStack (alignment: .center , spacing: 19){
            Text(title)
                .bold()
            Image(systemName: imageSystemName)
        }
        .frame(width: 130, height: 40, alignment: .center)
        .font(.title2)
        .background(buttonColor)
        .cornerRadius(10)
        .foregroundColor(.white)
    }
}

struct ButtonPayView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonPayView(title: "Pay", imageSystemName: "checkerboard.shield", buttonColor: .green)
    }
}
