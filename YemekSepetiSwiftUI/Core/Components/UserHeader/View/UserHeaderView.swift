//
//  UserHeaderView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 5.09.2022.
//

import SwiftUI

struct UserHeaderView: View {
    let userImage : String
    let userName : String
    var body: some View {
        VStack {
            Image(userImage)
                .resizable()
                .frame(width: 100, height: 100)
                .background(Color.yellow)
                .clipShape(Circle())
                .padding(.bottom, 10)
                .overlay (
                        Circle()
                            .stroke(.pink, lineWidth: 10)
                )
            Text(userName)
                .font(.system(size: 20))
                .fontWeight(.semibold)
                .foregroundColor(.gray)
        }
    }
}

struct UserHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        UserHeaderView(userImage: "user_p", userName: "John Appleseed")
    }
}
