//
//  TextFieldRowView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 30.08.2022.
//

import SwiftUI

struct TextFieldRowView: View {
    let placeholder : String
    let radius : CGFloat
    @Binding var bindingText : String
    
    var body: some View {
        TextField(placeholder, text: $bindingText)
            .padding()
            .background(Color.white)
            .cornerRadius(radius)
    }
}

struct TextFieldRowView_Previews: PreviewProvider {
    static var previews: some View {
        TextFieldRowView(placeholder: "Email", radius: 15, bindingText: .constant(""))
    }
}
