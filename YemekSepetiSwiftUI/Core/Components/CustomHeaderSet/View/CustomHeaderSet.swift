//
//  CustomHeaderSet.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 1.09.2022.
//

import SwiftUI

struct CustomHeaderSet: View {
    @Environment (\.presentationMode) var presentationMode
    let headerSmallText : String
    let headerLogo : String
    let isButtonVisible : Bool
    var body: some View {
        VStack {
            HStack {
                if isButtonVisible {
                    Button {
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        BackButtonView(backButtonString: "arrow.left")
                    }
                    Spacer()
                }else {
                    Spacer()
                }
                Text(headerSmallText)
                    .font(.caption)
                    .foregroundColor(.gray)
                Spacer()
                Image(headerLogo)
                    .resizable()
                    .frame(width: 40, height: 40)
                    .clipShape(Circle())

            }
        }
    }
}

struct CustomHeaderSet_Previews: PreviewProvider {
    static var previews: some View {
        CustomHeaderSet(headerSmallText: "Food Detail", headerLogo: "logo", isButtonVisible: true)
    }
}
