//
//  TopBarView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 26.09.2022.
//

import SwiftUI

struct TopBarView: View {
    @Binding var selected : Int
    
    var body: some View {
        
        HStack {
            // first button //
            Button {
                self.selected = 0
            } label: {
                Image(systemName: "star.fill")
                    .font(.title)
                    .frame(width: 25, height: 25)
                    .padding(.vertical, 12)
                    .padding(.horizontal, 30)
                    .background(self.selected == 0 ? .white : .clear)
                    .clipShape(Capsule())
            }
            .foregroundColor(self.selected == 0 ? .pink : .gray)
            
            // second button //
            Button {
                self.selected = 1
            } label: {
                Image(systemName: "star.bubble.fill")
                    .font(.title)
                    .frame(width: 25, height: 25)
                    .padding(.vertical, 12)
                    .padding(.horizontal, 30)
                    .background(self.selected == 1 ? .white : .clear)
                    .clipShape(Capsule())
            }
            .foregroundColor(self.selected == 1 ? .pink : .gray)

        }
        .padding(8)
        .background(Color(.systemGray4))
        .clipShape(Capsule())
    }

}

struct TopBarView_Previews: PreviewProvider {
    static var previews: some View {
        TopBarView(selected: .constant(0))
    }
}
