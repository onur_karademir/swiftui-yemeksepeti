//
//  BillingUserView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 7.09.2022.
//

import SwiftUI

struct BillingUserView: View {
    let billingUserTitle : String
    let billingUserAdress : String
    var body: some View {
        VStack (alignment: .leading, spacing: 9){
            HStack {
                Text(billingUserTitle)
                    .font(.title3)
                    .foregroundColor(.black)
                    .fontWeight(.bold)
            }
            Text("Adress:")
                .underline()
                .font(.caption2)
                .frame(height: 0)
            Text(billingUserAdress)
                .font(.subheadline)
                .foregroundColor(.black)
                .fontWeight(.bold)
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding()
        .background(Color.pink).opacity(0.4)
    }

}

struct BillingUserView_Previews: PreviewProvider {
    static var previews: some View {
        BillingUserView(billingUserTitle: "Billing address :", billingUserAdress: "Simply dummy text of the printing and typesetting industry.")
    }
}
