//
//  CommentRowView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 26.09.2022.
//

import SwiftUI

struct CommentRowView: View {
    let items: [Comment] = [.example1, .example2, .example3]
    @State private var flags: [Bool] = [true, false, false]
    var body: some View {
        VStack {
            ScrollView {
                ForEach(Array(items.enumerated()), id: \.1.id) { index, item in
                    VStack (alignment: .leading) {
                        HStack {Spacer()}
                        HStack {
                            Text(item.name)
                                .foregroundColor(self.flags[index] ? .white : .black)
                                .font(.title3)
                                .fontWeight(.semibold)
                            Spacer()
                            Image(systemName: self.flags[index] ? "chevron.up" : "chevron.down")
                                .foregroundColor(self.flags[index] ? .white : .black)
                        }
                            
                    }
                    .padding()
                    .background(self.flags[index] ? .pink : Color(.systemGray3))
                    .cornerRadius(20)
                    .onTapGesture {
                        withAnimation {
                            self.flags[index].toggle()
                        }
                   }
                    ForEach( item.items ?? [] ) { comment in
                        if self.flags[index] {
                            withAnimation {
                                VStack (alignment: .leading) {
                                    HStack (alignment:.top) {
                                        Image(systemName: comment.icon)
                                            .foregroundColor(.white)
                                            .font(.title3)
                                            .padding(9)
                                            .background(.blue)
                                            .clipShape(Circle())
                                        VStack (alignment: .leading, spacing: 15) {
                                            HStack {
                                                Text(comment.name)
                                                    .font(.subheadline)
                                                    .fontWeight(.semibold)
                                                Spacer()
                                                Text(comment.date)
                                                    .font(.footnote)
                                                    .fontWeight(.ultraLight)
                                                    .italic()
                                                    .underline()
                                            }
                                            Text(comment.comment)
                                        }
                                    }
                                }
                                .padding()
                                .background(Color(.systemGray5))
                                .cornerRadius(20)
                            }
                        }
                    }
                }
            }
            Spacer()
//            VStack {
//                ForEach(Array(items.enumerated()), id: \.offset) { index, item in
//                    Button {
//                    numberTab = index
//                        //items.firstIndex(where: {$0.name == item.name}) ?? 0
//                        print(numberTab)
//                        print(items[numberTab])
//                    } label: {
//                        Text(item.name)
//                    }
//                }
//                if numberTab == 0 {
//                    ForEach(items[0].items ?? []) { comment in
//                        Text(comment.name)
//                    }
//                } else if numberTab == 1 {
//                    ForEach(items[1].items ?? []) { comment in
//                        Text(comment.name)
//                    }
//                } else if numberTab == 2 {
//                    ForEach(items[2].items ?? []) { comment in
//                        Text(comment.name)
//                    }
//                } else {
//                    Text("")
//                }
//            }
//            .onAppear {
//                numberTab = 4
//            }
        }
    }
}

struct CommentRowView_Previews: PreviewProvider {
    static var previews: some View {
        CommentRowView()
    }
}
