//
//  CommentRowViewModel.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 27.09.2022.
//

import Foundation

struct Comment: Identifiable {
    let id = UUID()
    let comment : String
    let name : String
    let icon : String
    let date : String
    var items : [Comment]?

    // some example //
    static let comment1 = Comment(
        comment: "Food 1 simply dummy text of the printing and typesetting industry.11",
        name: "Comment - 1",
        icon: "1.circle",
        date: "05.06.2022"
    )
    
    static let comment2 = Comment(
        comment: "Food 1 simply dummy text of the printing and typesetting industry.22",
        name: "Comment - 2",
        icon: "square.and.pencil",
        date: "02.04.2022"
    )
    static let comment3 = Comment(
        comment: "Food 1 simply dummy text of the printing and typesetting industry.33",
        name: "Comment - 3",
        icon: "bolt.fill",
        date: "01.07.2022"
    )
    static let comment4 = Comment(comment: "Food 1 simply dummy text of the printing and typesetting industry.44", name: "Comment - 4", icon: "mic", date: "08.10.2022")
    
    static let comment5 = Comment(
        comment: "Food 1 simply dummy text of the printing and typesetting industry.55",
        name: "Comment - 5",
        icon: "mic",
        date: "02.03.2022"
    )
    static let comment6 = Comment(
        comment: "Food 1 simply dummy text of the printing and typesetting industry.66",
        name: "Comment - 6",
        icon: "bolt.fill",
        date: "05.07.2022"
    )
    static let comment7 = Comment(
        comment: "Food 1 simply dummy text of the printing and typesetting industry.77",
        name: "Comment - 7",
        icon: "square.and.pencil",
        date: "04.05.2022"
    )
    
    static let comment8 = Comment(
        comment: "Food 1 simply dummy text of the printing and typesetting industry.88",
        name: "Comment - 8",
        icon: "mic",
        date: "01.09.2022"
    )
    static let comment9 = Comment(
        comment: "Food 1 simply dummy text of the printing and typesetting industry.99",
        name: "Comment - 9",
        icon: "bolt.fill",
        date: "09.16.2022"
    )
    static let comment10 = Comment(
        comment: "Food 1 simply dummy text of the printing and typesetting industry.1010",
        name: "Comment - 10",
        icon: "square.and.pencil",
        date: "10.12.2022"
    )
    

    // some example groups
    static let example1 = Comment(
        comment: "Food 1 simply dummy text of the printing and typesetting industry.",
        name: "Restourant - 1", 
        icon: "star",
        date: "05.06.2022",
        items: [Comment.comment1, Comment.comment2, Comment.comment3, Comment.comment4]
    )
    
    static let example2 = Comment(
        comment: "Food 1 simply dummy text of the printing and typesetting industry.",
        name: "Restourant - 2",
        icon: "timer",
        date: "05.06.2022",
        items: [Comment.comment5, Comment.comment6, Comment.comment7]
    )
    
    static let example3 = Comment(
        comment: "Food 1 simply dummy text of the printing and typesetting industry.",
        name: "Restourant - 3",
        icon: "timer",
        date: "05.06.2022",
        items: [Comment.comment8, Comment.comment9, Comment.comment10]
    )
}
