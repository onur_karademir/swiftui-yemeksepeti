//
//  BillingAdressVievModel.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 7.09.2022.
//

import Foundation


struct BillingsModel : Identifiable {
    var id = UUID().uuidString
    var key : String
    var value : String
}

var billingArray = [
    BillingsModel(key: "Billing - 1", value: "Simply dummy text of the printing and typesetting industry."),
    BillingsModel(key: "Billing - 2", value: "Simply dummy text of the printing and typesetting industry."),
    BillingsModel(key: "Billing - 3", value: "Simply dummy text of the printing and typesetting industry.")
]
