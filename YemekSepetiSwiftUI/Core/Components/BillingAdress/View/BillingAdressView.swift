//
//  BillingAdressView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 7.09.2022.
//

import SwiftUI
import HalfASheet

struct BillingAdressView: View {
    let billingTitle : String
    let billingIcon : String
    let billingSecondIcon : String
    let billingChangeText : String
    var billingAdress : String
    @State var isOpenBilling : Bool = false
    @State var billingString : String = ""
    var body: some View {
        VStack (alignment: .leading, spacing: 9) {
            HStack {
                Text(billingTitle)
                    .font(.title3)
                    .foregroundColor(.gray)
                Image(systemName: billingIcon)
                    .foregroundColor(.gray)
                Spacer()
                HStack {
                    Image(systemName: billingSecondIcon)
                        .foregroundColor(.gray)
                        .font(.caption2)
                    Text(billingChangeText)
                        .underline()
                        .foregroundColor(.gray)
                        .font(.caption2)
                }
                .onTapGesture {
                    isOpenBilling.toggle()
                }
                .sheet(isPresented: $isOpenBilling) {
                    BillingChangeView(billingString: $billingString)
                }
            }
            if !billingString.isEmpty {
                Text(billingString)
                    .font(.subheadline)
            } else {
                Text(billingAdress)
                    .font(.subheadline)
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding()
        .background(Color(red: 242 / 255, green: 242 / 255, blue: 242 / 255))
    }
}

struct BillingAdressView_Previews: PreviewProvider {
    static var previews: some View {
        BillingAdressView(billingTitle: "Billing address :", billingIcon: "highlighter", billingSecondIcon: "pencil.and.outline", billingChangeText: "Change", billingAdress: "Simply dummy text of the printing and typesetting industry.")
    }
}
