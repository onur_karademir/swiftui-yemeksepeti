//
//  WalletTextView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 13.09.2022.
//

import SwiftUI

struct WalletTextView: View {
    let walletTextString : String
    let walletSmallTitle : String
    var body: some View {
        VStack (alignment: .leading, spacing: 9){
            Text(walletSmallTitle)
                .font(.caption)
                .foregroundColor(.gray)
                .italic()
                .underline()
            Text(walletTextString)
                .font(.callout)
        }
        .padding()
        //.background(Color(.systemGray5))
        //.border(.purple, width: 2)
    }
}

struct WalletTextView_Previews: PreviewProvider {
    static var previews: some View {
        WalletTextView(walletTextString: "The wallet is designed to make your payments easier.You can load any amount you want and spend whenever you want. 😀", walletSmallTitle: "Wallet:")
    }
}
