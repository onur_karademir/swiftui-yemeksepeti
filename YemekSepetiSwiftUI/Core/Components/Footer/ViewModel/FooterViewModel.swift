//
//  FooterViewModel.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 15.09.2022.
//

import Foundation

struct FooterDataModel : Identifiable {
    let id = UUID()
    let title : String
}

let footerArray = [
    FooterDataModel(title: "Istanbul"),
    FooterDataModel(title: "Izmir"),
    FooterDataModel(title: "Ankara"),
    FooterDataModel(title: "Adana"),
    FooterDataModel(title: "Bursa"),
    FooterDataModel(title: "Erzurum"),
    FooterDataModel(title: "Eskisehir"),
    FooterDataModel(title: "Antalya"),
    FooterDataModel(title: "Samsun"),
    FooterDataModel(title: "Trabzon"),
    FooterDataModel(title: "Rize"),
    FooterDataModel(title: "Sivas")
]
