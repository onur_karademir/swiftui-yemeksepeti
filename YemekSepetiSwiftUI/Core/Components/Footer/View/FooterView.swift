//
//  FooterView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 15.09.2022.
//

import SwiftUI

struct FooterView: View {
    var body: some View {
        VStack (alignment: .leading){
            
            HStack {
                MiniLogoView(miniLogoText: "YemekPaketi Tic.A.S")
                Spacer()
                Text("2022 All Rights Reserved")
                    .underline()
                    .italic()
                    .font(.footnote)
                    .foregroundColor(.gray)
            }
            
            LazyVGrid(columns: [GridItem(.adaptive(minimum: 100))]
                      ,alignment: .leading) {
                ForEach(footerArray,id:\.id) { item in
                    HStack {
                        Text(item.title)
                            .underline()
                            .italic()
                            .font(.footnote)
                            .foregroundColor(.gray)
                    }
                    .padding(.leading, 7)
                    .padding(.top, 0.1)
                }
            }
            HStack {
                
                PayMethodView(payMethodText: "Pay & Method", payMethodImg: "mcardv", payMethodImgVisa: "card1")
                
                Spacer()
                
                SocialMediaView(socialMediaText: "Social Media")
                
            }
            .padding(.leading, 7)
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding(.horizontal)
        .padding(.vertical, 8)
        .background(Color.white)
    }
}

struct FooterView_Previews: PreviewProvider {
    static var previews: some View {
        FooterView()
    }
}
