//
//  SmallCardView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 30.08.2022.
//

import SwiftUI

struct SmallCardView: View {
    @AppStorage("is_market") var isMarket : Bool = false
    var cardData : CardModel = cardArray[2]
    var body: some View {
        VStack (alignment: .leading, spacing: 8){
            Image(cardData.image)
                .resizable()
                .scaledToFill()
                .frame(height: 100)
                .clipped()
        }
        .cornerRadius(20)
        .shadow(color: isMarket ? .pink : .clear, radius: 30, x: 0, y: 5)
        .overlay(
            Circle()
                .fill(isMarket ? Color.pink : Color.white)
                .frame(width: 20,height: 20)
                .padding(5)
                .overlay(
                    VStack (alignment: .center, spacing: 0){
                        Text(cardData.line)
                            .font(.caption)
                            .fontWeight(.semibold)
                            .foregroundColor(isMarket ? Color.white : Color.black)
                    }
                )
           
            ,alignment: .topTrailing
        )
        .overlay(
            Text(isMarket ? "" : cardData.title)
                .underline()
                .font(.subheadline)
                .foregroundColor(.white)
                .fontWeight(.semibold)
            ,alignment: .center
        )
    }

}

struct SmallCardView_Previews: PreviewProvider {
    static var previews: some View {
        SmallCardView()
    }
}
