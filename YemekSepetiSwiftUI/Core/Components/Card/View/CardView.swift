//
//  CardView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 30.08.2022.
//

import SwiftUI

struct CardView: View {
    @AppStorage("is_market") var isMarket : Bool = false
    var cardData : CardModel = cardArray[2]
    var body: some View {
        VStack (alignment: .leading, spacing: 8){
            Image(cardData.image)
                .resizable()
                .scaledToFill()
                .frame(width: isMarket ? 100 : 200, height: isMarket ? 150 : 250)
                .clipped()
        }
        .cornerRadius(20)
        //.shadow(radius: isMarket ? 10 : 0)
        .overlay(
            Circle()
                .fill(isMarket ? Color.pink : Color.white)
                .frame(width: isMarket ? 40 : 50,height: isMarket ? 40 : 50)
                .padding(5)
                .overlay(
                    VStack (alignment: .center, spacing: 0){
                        Text(cardData.minute)
                            .font(isMarket ? .caption2 : .title3)
                            .fontWeight(.semibold)
                            .foregroundColor(isMarket ? .white : Color.black)
                        Text("Dk")
                            .font(isMarket ? .caption2 : .subheadline)
                            .fontWeight(.semibold)
                            .foregroundColor(isMarket ? .white : Color.black)
                    }
                )
           
            ,alignment: .topTrailing
        )
        .overlay(
            Circle()
                .fill(isMarket ? Color.pink : Color.white)
                .frame(width: isMarket ? 40 : 50,height: isMarket ? 40 : 50)
                .padding(5)
                .overlay(
                    VStack (alignment: .center, spacing: 0){
                        Text("No.")
                            .font(isMarket ? .caption2 : .subheadline)
                            .fontWeight(.semibold)
                            .foregroundColor(isMarket ? .white : Color.black)
                        Text(cardData.line)
                            .font(isMarket ? .caption2 : .title3)
                            .fontWeight(.semibold)
                            .foregroundColor(isMarket ? .white : Color.black)
                    }
                )
           
            ,alignment: .bottomLeading
        )
        .overlay(
            Text(isMarket ? "" : cardData.title)
                .underline()
                .font(isMarket ? .footnote : .largeTitle)
                .foregroundColor(isMarket ? .black : .white)
                .fontWeight(.semibold)
            ,alignment: .center
        )
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView()
    }
}
