//
//  CardViewModel.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 30.08.2022.
//

import Foundation
import SwiftUI

struct CardModel : Identifiable {
    var id = UUID()
    var title : String
    var minute : String
    var line : String
    var image : String
    var orderCount : String
    var visitCount : String
    var desc : String
    var restName : String
    var allFoods : [String]
    var price : String
    var discount : String
    var icon : String
    var color : Color
}

let cardArray = [
    CardModel(title: "Food 1", minute: "5", line: "1", image: "food1", orderCount : "1120", visitCount : "188", desc : "Food 1 simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", restName: "restaurant - 1", allFoods: ["Menu 1","Menu 2","Menu 3"], price: "44.99 TL", discount: "29.99 TL", icon: "house", color: .pink),
    
    CardModel(title: "Food 2", minute: "10", line: "2", image: "food2", orderCount : "999", visitCount : "128", desc : "Food 2 simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", restName: "restaurant - 2", allFoods: ["Menu 1","Menu 2","Menu 3"], price: "39.99 TL", discount: "19.99 TL", icon: "bolt.heart", color: .yellow),
    
    CardModel(title: "Food 3", minute: "15", line: "3", image: "food3", orderCount : "888", visitCount : "139", desc : "Food 4 simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", restName: "restaurant - 3", allFoods: ["Menu 1","Menu 2","Menu 3"], price: "54.99 TL", discount: "24.99 TL", icon: "airplane.departure", color: .green),
    
    CardModel(title: "Food 4", minute: "20", line: "4", image: "food4", orderCount : "765", visitCount : "99", desc : "Food 4 simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", restName: "restaurant - 3", allFoods: ["Menu 1","Menu 2","Menu 3"], price: "59.99 TL", discount: "49.99 TL", icon: "play.slash.fill", color: .orange),
    
    CardModel(title: "Food 5", minute: "25", line: "5", image: "food5", orderCount : "345", visitCount : "88", desc : "Food 5 simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", restName: "restaurant - 4", allFoods: ["Menu 1","Menu 2","Menu 3"], price: "44.99 TL", discount: "29.99 TL", icon: "cup.and.saucer", color: .indigo),
    
    CardModel(title: "Food 6", minute: "30", line: "6", image: "food6", orderCount : "120", visitCount : "67", desc : "Food 6 simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", restName: "restaurant - 5", allFoods: ["Menu 1","Menu 2","Menu 3"], price: "44.99 TL", discount: "29.99 TL", icon: "crown.fill", color: .mint)
]


let cardMarketArray = [
    CardModel(title: "Market Item 1", minute: "5", line: "1", image: "market1", orderCount : "1120", visitCount : "188", desc : "Market Item 1 simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", restName: "Market - 1", allFoods: ["Market Item 1","Market Item 2","Market Item 3"], price: "44.99 TL", discount: "29.99 TL", icon: "fanblades.fill", color: .mint),
    
    CardModel(title: "Market Item 2", minute: "10", line: "2", image: "market2", orderCount : "999", visitCount : "128", desc : "Market Item 2 simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", restName: "Market - 2", allFoods: ["Market Item 1","Market Item 2","Market Item 3"], price: "39.99 TL", discount: "19.99 TL", icon: "crown.fill", color: .indigo),
    
    CardModel(title: "Market Item 3", minute: "15", line: "3", image: "market3", orderCount : "888", visitCount : "139", desc : "Market Item 4 simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", restName: "Market - 3", allFoods: ["Market Item 1","Market Item 2","Market Item 3"], price: "54.99 TL", discount: "24.99 TL", icon: "cup.and.saucer", color: .orange),
    
    CardModel(title: "Market Item 4", minute: "20", line: "4", image: "market4", orderCount : "765", visitCount : "99", desc : "Market Item 4 simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", restName: "Market - 3", allFoods: ["Market Item 1","Market Item 2","Market Item 3"], price: "59.99 TL", discount: "49.99 TL", icon: "fork.knife", color: .green),
    
    CardModel(title: "Market Item 5", minute: "25", line: "5", image: "market5", orderCount : "345", visitCount : "88", desc : "Market Item 5 simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", restName: "Market - 4", allFoods: ["Market Item 1","Market Item 2","Market Item 3"], price: "44.99 TL", discount: "29.99 TL", icon: "takeoutbag.and.cup.and.straw.fill", color: .pink),
    
    CardModel(title: "Market Item 6", minute: "30", line: "6", image: "market6", orderCount : "120", visitCount : "67", desc : "Market Item 6 simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.", restName: "Market - 5", allFoods: ["Market Item 1","Market Item 2","Market Item 3"], price: "44.99 TL", discount: "29.99 TL", icon: "fork.knife.circle.fill", color: .yellow)
]
