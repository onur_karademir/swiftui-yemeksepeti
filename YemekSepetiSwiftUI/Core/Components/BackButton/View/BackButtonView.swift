//
//  BackButtonView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 1.09.2022.
//

import SwiftUI

struct BackButtonView: View {
    let backButtonString : String
    var body: some View {
        Image(systemName: backButtonString)
            .foregroundColor(.white)
            .font(.headline)
            .padding(.horizontal)
            .padding(.vertical, 8)
            .background(Color.pink)
            .clipShape(Capsule())
    }
}

struct BackButtonView_Previews: PreviewProvider {
    static var previews: some View {
        BackButtonView(backButtonString: "arrow.left")
    }
}
