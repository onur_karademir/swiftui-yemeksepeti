//
//  BestSellerTextView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 30.08.2022.
//

import SwiftUI

struct BestSellerTextView: View {
    var bestSellerText : String
    var body: some View {
        Text(bestSellerText)
            .font(.title3)
            .fontWeight(.semibold)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.leading)
            .padding(.top)
    }
}

struct BestSellerTextView_Previews: PreviewProvider {
    static var previews: some View {
        BestSellerTextView(bestSellerText: "Best Seller Menu")
    }
}
