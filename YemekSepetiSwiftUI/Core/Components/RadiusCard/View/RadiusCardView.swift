//
//  RadiusCardView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 20.09.2022.
//

import SwiftUI

struct RadiusCardView: View {
    var cardData : CardModel = cardArray[0]
    var body: some View {
        VStack {
            Image(cardData.image)
                .resizable()
                .scaledToFill()
                .clipShape(Circle())
                .frame(width: 160, height: 160, alignment: .center)
                .overlay(
                        Circle()
                            .stroke(cardData.color, lineWidth: 7)
                )
                .overlay(
                    Text(cardData.title)
                        .foregroundColor(.white)
                        .font(.footnote)
                        .italic()
                        .underline()
                        .padding(.vertical,5)
                        .padding(.horizontal,10)
                        .background(.pink)
                        .cornerRadius(20)
                    ,alignment: .center
                )
                .overlay(
                    Text(cardData.line)
                        .foregroundColor(.white)
                        .font(.subheadline)
                        .fontWeight(.semibold)
                        .padding(8)
                        .background(.black)
                        .cornerRadius(20)
                        .clipShape(Circle())
                    ,alignment: .topTrailing
                )
                .overlay(
                    Text(cardData.discount)
                        .foregroundColor(.white)
                        .font(.footnote)
                        .fontWeight(.semibold)
                        .padding(7)
                        .background(cardData.color)
                        .cornerRadius(20)
                        .clipShape(Capsule())
                    ,alignment: .bottomLeading
                )
        }
        .padding(5)
    }
}

struct RadiusCardView_Previews: PreviewProvider {
    static var previews: some View {
        RadiusCardView()
    }
}
