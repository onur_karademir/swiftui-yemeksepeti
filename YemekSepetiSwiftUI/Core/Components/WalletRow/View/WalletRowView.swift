//
//  WalletRowView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 13.09.2022.
//

import SwiftUI

struct WalletRowView: View {
    @AppStorage("selected_color") var selectedColor : String = ""
    // local varible //
    let walletRowTitle : String
    let walletRowImg : String
    let walletRowNoTitle : String
    // state varibles //
    @State var isOpenDetail : Bool = false
    @State var isAddPaketMoney : Bool = false
    @State var isSelectedWallet : Color = .pink
    @State var walletBalance : String = ""
    @State var walletItem : String = ""
    @State var sum : Int = 0
    // app storage //
    @AppStorage("pm_point") var currentUserPm : Int = 0
    @AppStorage("user_name") var currentUserName: String?
    // focus state //
    @FocusState private var numberIsFocused: Bool
    // stateObject
    @StateObject var vm = CoreDataUserViewModel()
    
    func chooseColor() {
        if selectedColor == "pink" {
            isSelectedWallet = .pink
        }else if selectedColor == "indigo" {
            isSelectedWallet = .indigo
        }else if selectedColor == "orange" {
            isSelectedWallet = .orange
        }else if selectedColor == "black" {
            isSelectedWallet = .black
        }else if selectedColor == "cyan" {
            isSelectedWallet = .cyan
        }else {
            isSelectedWallet = .pink
        }
    }
    
    var body: some View {
        VStack {
            ZStack (alignment: .topTrailing) {
                
                VStack {
                    HStack{Spacer()}
                    HStack (alignment: .center) {
                        VStack (spacing: 10) {
                            Text(walletRowTitle)
                                .font(.title2)
                                .fontWeight(.semibold)
                                .foregroundColor(.white)
                            Image(systemName: walletRowImg)
                                .font(.system(size: 40, weight: .semibold))
                                .foregroundColor(.white)
                        }
                        VStack (spacing: 5) {
                            Text(currentUserName ?? "user")
                                .font(.footnote)
                                .fontWeight(.semibold)
                                .foregroundColor(.white)
                            Text(walletRowNoTitle)
                                .font(.footnote)
                                .fontWeight(.semibold)
                                .foregroundColor(.white)
                                .underline()
                            Text(" 75xx - xxxx - xxxx - xx99 ")
                                .font(.callout)
                                .foregroundColor(.white)
                                .fontWeight(.semibold)
                                .underline()
                        }
                    }
                }
                .onAppear{
                    chooseColor()
                }
                .padding(.vertical, 9)
                .background(isSelectedWallet)
                Button {
                    isOpenDetail.toggle()
                } label: {
                    Text("Detail")
                        .fontWeight(.semibold)
                        .padding(.vertical, 4)
                        .padding(.horizontal, 6)
                        .background(.green)
                        .foregroundColor(.white)
                        .font(.footnote)
                        //.clipShape(Capsule())
                }
                .sheet(isPresented: $isOpenDetail) {
                    WalletDetailView(isSelectedWallet: $isSelectedWallet)
                }
            }
            
            HStack (alignment: .top){
                VStack (spacing: 10){
                    Text("Remaining balance:")
                        .font(.callout)
                    Text("\(currentUserPm) PM")
                        .foregroundColor(currentUserPm >= 100 ? .green : .red)
                        .fontWeight(.semibold)
                }
                VStack {
                    TextField("Add PaketMoney", text: $walletItem)
                        .focused($numberIsFocused)
                        .keyboardType(.numberPad)
                        .padding(2)
                        .padding(.horizontal)
                        .background(Color(.systemGray4))
                        .cornerRadius(8)
                    Button {
                        isAddPaketMoney = true
                        numberIsFocused = false
                        // onay kismi yok iken bu sekilde idi
//                        sum += Int(walletItem) ?? 0
//                        walletItem = ""
                        //??
                        //sum = (Int(walletBalance) ?? 0) + (Int(walletItem) ?? 0)
                    } label: {
                        Text("Add")
                            .foregroundColor(.white)
                            .font(.callout)
                            .padding(.horizontal)
                            .padding(.vertical, 7)
                            .background(.pink)
                            .clipShape(Capsule())
                    }
                    .disabled(walletItem.count >= 1 ? false : true)
                    .opacity(walletItem.count >= 1 ? 1 : 0.5)
                    .confirmationDialog("Are you sure?",
                      isPresented: $isAddPaketMoney, titleVisibility: .visible) {
                      Button("Add \(walletItem) PaketMoney?", role: .destructive) {
                          currentUserPm += Int(walletItem) ?? 0
                          vm.addPaketMoney(paket_money: currentUserPm, current_user: currentUserName ?? "user")
                          walletItem = ""
                       }
                     }
                }
                    
            }
            .padding(.horizontal)
        }
        .onTapGesture {
            numberIsFocused = false
        }
    }
}

struct WalletRowView_Previews: PreviewProvider {
    static var previews: some View {
        WalletRowView(walletRowTitle: "Wallet", walletRowImg: "menucard", walletRowNoTitle: "Wallet No:")
    }
}
