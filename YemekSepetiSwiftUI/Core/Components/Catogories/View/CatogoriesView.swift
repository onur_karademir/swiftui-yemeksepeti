//
//  CatogoriesView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 5.09.2022.
//

import SwiftUI

struct CatogoriesView: View {
    var cardData : CardModel = cardArray[0]
    let Width : CGFloat
    let Height : CGFloat
    let IconSize : CGFloat
    let fontSize : Font
    let colorType : Color
    var body: some View {
        VStack (spacing: 8){
            Circle()
                .fill(colorType)
                .frame(width: Width, height: Height, alignment: .center)
                .overlay(
                    Image(systemName: cardData.icon)
                        .foregroundColor(Color.white)
                        .font(.system(size: IconSize, weight: .bold))
                        .padding(0)
                )
            Text(cardData.title)
                .font(fontSize)
                .fontWeight(.bold)
                .foregroundColor(.black)
        }
        //.shadow(radius: 20)
        //.background(Color(red: 242 / 255, green: 242 / 255, blue: 242 / 255))
    }
}

struct CatogoriesView_Previews: PreviewProvider {
    static var previews: some View {
        CatogoriesView(Width: 100, Height: 100, IconSize: 45, fontSize: .title2, colorType: Color.orange)
    }
}
