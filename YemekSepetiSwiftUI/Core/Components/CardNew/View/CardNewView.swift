//
//  CardNewView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 1.10.2022.
//

import SwiftUI

struct CardNewView: View {
    @AppStorage("is_market") var isMarket : Bool = false
    var cardData : CardModel = cardArray[2]
    var body: some View {
        VStack (spacing: 15) {
            
            Image(cardData.image)
                .resizable()
                .scaledToFill()
                .frame(width: 130, height: 130, alignment: .center)
                .clipShape(Circle())
                .overlay (
                        Circle()
                            .stroke(isMarket ? .orange : .orange, lineWidth: 6)
                )
            Text(cardData.restName)
                .font(.title3)
                .fontWeight(.semibold)
                .foregroundColor(.black)
            
            Text(cardData.title)
                .font(.subheadline)
                .fontWeight(.semibold)
                .foregroundColor(.black)
            HStack {
                Label(cardData.orderCount, systemImage: "cart.fill.badge.plus")
                    .font(.footnote)
                    .foregroundColor(.secondary)
                Label(cardData.visitCount, systemImage: "eye.fill")
                    .font(.footnote)
                    .foregroundColor(.secondary)
                VStack {
                    Text(cardData.price)
                        .strikethrough()
                        .foregroundColor(.red)
                        .font(.caption2)
                        .fontWeight(.bold)
                    Text(cardData.discount)
                        .foregroundColor(.green)
                        .font(.caption)
                        .fontWeight(.bold)
                }
            }
        }
        .frame(width: 160, height: 250, alignment: .top)
        .padding()
        .background(Color(.systemGray6))
        .cornerRadius(20)
        .overlay(
            VStack {
                Circle()
                    .fill(isMarket ? .orange : .pink)
                    .frame(width: 50, height: 50)
                    .padding(5)
                    .overlay(
                        VStack (spacing: 2) {
                            Image(systemName: "clock")
                                .font(.subheadline)
                                .foregroundColor(isMarket ? .black : .white)
                            HStack (alignment: .center, spacing: 3){
                                Text(cardData.minute)
                                    .font(.caption2)
                                    .fontWeight(.semibold)
                                    .foregroundColor(isMarket ? .black : .white)
                                Text("Dk")
                                    .font(.caption2)
                                    .fontWeight(.semibold)
                                    .foregroundColor(isMarket ? .black : .white)
                              }
                        }
                )
            }
            .offset(x: 15, y: -15)
            ,alignment: .topTrailing
        )
    }
}

struct CardNewView_Previews: PreviewProvider {
    static var previews: some View {
        CardNewView()
    }
}
