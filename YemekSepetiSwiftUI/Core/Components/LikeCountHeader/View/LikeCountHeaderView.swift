//
//  LikeCountHeaderView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 15.09.2022.
//

import SwiftUI

struct LikeCountHeaderView: View {
    let likeHeader : String
    let likeCount : String
    var body: some View {
        VStack {
            HStack {
                Image(systemName: "star.fill")
                    .symbolRenderingMode(.multicolor)
                    .font(.title)
                Text(likeHeader)
                    .foregroundColor(.white)
                    .font(.title2)
                    .fontWeight(.semibold)
                    .italic()
                Text(likeCount)
                    .foregroundColor(.white)
                    .font(.title2)
                    .fontWeight(.bold)
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding()
        .background(.pink)
    }
}

struct LikeCountHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        LikeCountHeaderView(likeHeader: "Number of favorites: ", likeCount: "1")
    }
}
