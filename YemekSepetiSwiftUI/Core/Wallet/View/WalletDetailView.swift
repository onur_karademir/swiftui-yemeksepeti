//
//  WalletDetailView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 21.09.2022.
//

import SwiftUI

struct WalletDetailView: View {
    // app storage //
    @AppStorage("selected_color") var selectedColor : String = ""
    @State var colorArray : [Color] = [.pink,.indigo,.orange,.black,.cyan]
    @State var colorIndexs : Int = 0
    @Binding var isSelectedWallet : Color
    
    @discardableResult func chooseColor(name: Color) -> String {
        switch name {
        case .pink:
            selectedColor = "pink"
            return "pink"
        case .indigo:
            selectedColor = "indigo"
            return "indigo"
        case .orange:
            selectedColor = "orange"
            return "orange"
        case .black:
            selectedColor = "black"
            return "black"
        case .cyan:
            selectedColor = "cyan"
            return "cyan"
        default :
            return ""
        }
    }
    
    var body: some View {
        VStack {
            PayPageHeaderView(closeButtonIcon: "xmark", headerLogo: "logo")
            ScrollView {
                VStack (spacing: 20) {
                    HStack{Spacer()}
                    Text("Select Wallet Color")
                        .font(.title2)
                    HStack (spacing: 20){
                        ForEach(Array(colorArray.enumerated()),id:\.offset) { index, item in
                            Circle()
                                .frame(width: 45, height: 45, alignment: .center)
                                .foregroundColor(item)
                                .onTapGesture {
                                    isSelectedWallet = item

                                        colorIndexs = colorArray.firstIndex(where: {$0 == item}) ?? 0
                                    chooseColor(name: isSelectedWallet)
                                    print("selected color -->> \(selectedColor)")
                                }
                                .overlay(
                                    Circle()
                                        .stroke(colorIndexs == index ? .green : .clear, lineWidth: colorIndexs == index ? 6 : 0)
                                )
                                .onAppear {
                                    colorIndexs = colorArray.firstIndex(where: {$0 == isSelectedWallet}) ?? 0
                                    chooseColor(name: isSelectedWallet)
                                }
                        }
                    }
                    VStack {
                        Circle()
                            .frame(width: 190, height: 190, alignment: .center)
                            .foregroundColor(isSelectedWallet)
                            .overlay(
                                VStack {
                                    Image(systemName: "menucard")
                                        .font(.system(size: 80, weight: .semibold))
                                            .foregroundColor(.white)
                                    Text("Vallet")
                                        .underline()
                                        .font(.largeTitle)
                                        .foregroundColor(.white)
                                }
                        )
                    }
                }
            }
        }
    }
}

struct WalletDetailView_Previews: PreviewProvider {
    static var previews: some View {
        WalletDetailView(isSelectedWallet: .constant(.clear))
    }
}
