//
//  WalletView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 13.09.2022.
//

import SwiftUI

struct WalletView: View {
    var body: some View {
        VStack {
            PayPageHeaderView(closeButtonIcon: "xmark", headerLogo: "logo")
            
            WalletHeaderView(walletText: "Wallet & Paket Money")
            
            WalletTextView(walletTextString: "The wallet is designed to make your payments easier. You can load any amount you want and spend whenever you want. 😀", walletSmallTitle: "Wallet:")
            
            Divider()
            Spacer()
            ScrollView {
                WalletRowView(walletRowTitle: "Wallet", walletRowImg: "menucard", walletRowNoTitle: "Wallet No:")
                    
                WalletTextView(walletTextString: "Load and spend PaketMoney whenever you want from your card. don't miss out on the benefits. 💵", walletSmallTitle: "PaketMoney:")
                //RegisterCardView(regCardTitle: "Registered Card", regCardImg: "card1", regCardNoTitle: "Card No:")
                Divider()
            }
        }
        .background(Color(.systemGray6))
    }
}

struct WalletView_Previews: PreviewProvider {
    static var previews: some View {
        WalletView()
    }
}
