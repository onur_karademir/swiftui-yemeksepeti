//
//  ProfileView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 5.09.2022.
//

import SwiftUI

struct ProfileView: View {
    @State var notificationToggle: Bool = false
    @State var locationUsage: Bool = false
    @State var isDeleteAccountConfirm : Bool = false
    @State var isLogOutConfirm : Bool = false
    @State var isDeletePMConfirm : Bool = false
    // select menu //
    @State var selectedCurrency: Int = 0
    @State var selectedPaymentMethod: Int = 1
    // menu selection //
    @State var currencyArray: [String] = ["₺ Turkish Lira", "$ US Dollar", "€ Euro"]
    @State var paymentMethodArray: [String] = ["Paypal", "Credit/Debit Card", "Bitcoin"]
    // app storage //
    @AppStorage("selected_color") var selectedColor : String = ""
    @AppStorage("signed_in") var currentUserSignedIn : Bool = false
    @AppStorage("user_name") var currentUserName: String?
    @AppStorage("pm_point") var currentUserPm : Int = 0
    @AppStorage("is_market") var isMarket : Bool = false
    // state object //
    @StateObject var vm = CoreDataUserViewModel()
    var body: some View {
        VStack {
            
            UserHeaderView(userImage: "user_p", userName: currentUserName ?? "User Name")
            RegisterCardView(regCardTitle: "Registered Card", regCardImg: "card1", regCardNoTitle: "Card No:")
                .padding(.top)
            
            Form {
                Section(header: Text("Payment Settings")) {
                    
                    PaymentSettingsView(sectionName: "Currency", selected: $selectedCurrency, currencyArray: $currencyArray)
                    
                    PaymentSettingsView(sectionName: "Payment Method", selected: $selectedPaymentMethod, currencyArray: $paymentMethodArray)
                    
                }
                
                Section(header: Text("Personal Information")) {
                    
                    DynamicNavLInkView(content: { ProfileInfoView() }, navText: "Profile Info")
                    
                    DynamicNavLInkView(content: { BillingInfoView() }, navText: "Billing Info")
                    
                    Button {
                        isMarket = false
                        isLogOutConfirm = true
                        selectedColor = "pink"
                    } label: {
                        Text("Log Out")
                            .foregroundColor(.blue)
                    }
                    .confirmationDialog("Are you sure you want to LOGOUT account?",
                      isPresented: $isLogOutConfirm, titleVisibility: .visible) {
                        Button("Are you sure Logout?", role: .destructive) {
                            currentUserSignedIn = false
                        }
                        
                    }
                    
                    Button {
                        isDeletePMConfirm = true
                    } label: {
                        Text("Reset PaketMoney (PM)")
                            .foregroundColor(.pink)
                    }
                    .confirmationDialog("Are you sure you want to RESET your PM?",
                      isPresented: $isDeletePMConfirm, titleVisibility: .visible) {
                        Button("Are you sure RESET \(currentUserPm) PM?", role: .destructive) {
                            currentUserPm = 0
                        }
                        
                    }
                    
                    Button {
                        isDeleteAccountConfirm = true
                    } label: {
                        Text("Delete Account")
                            .foregroundColor(.red)
                    }
                    .confirmationDialog("Are you sure you want to DELETE your account?",
                      isPresented: $isDeleteAccountConfirm, titleVisibility: .visible) {
                        Button("Delete Account?", role: .destructive) {
                            vm.deleteUser()
                        }
                        
                    }
                }
                
                Section(footer: Text("Allow push notifications to get latest travel and equipment deals")) {
                    Toggle(isOn: self.$locationUsage) {
                          Text("Location Usage")
                    }
                    Toggle(isOn: self.$notificationToggle) {
                        Text("Notifications")
                    }
                }
            }.background(Color(red: 242 / 255, green: 242 / 255, blue: 242 / 255))
        }
        .navigationTitle("")
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
