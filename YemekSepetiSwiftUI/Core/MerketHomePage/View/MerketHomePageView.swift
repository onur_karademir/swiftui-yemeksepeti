//
//  MerketHomePageView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 16.09.2022.
//

import SwiftUI

struct MerketHomePageView: View {
    //presentationMode var
    @Environment (\.presentationMode) var presentationMode
    //state value
    @State var searchText = ""
    @State var isOpenWalletMenu : Bool = false
    @State var isOpenPayMenu : Bool = false
    @State var isOpenLikeMenu : Bool = false
    @State private var index = 0
    
    //app storage
    @AppStorage("index_number") var indexNumber : Int = 0
    @AppStorage("index_data_array") var indexArray : Int = 0
    @AppStorage("selected_menu") var selectedMenu : String = ""
    @AppStorage("is_market") var isMarket : Bool = false
    // state object //
    @StateObject var vm : CoreDataUserViewModel = CoreDataUserViewModel()
    
    var body: some View {
        VStack (alignment: .leading) {
            marketHeaderView
            marketCaroselView
            marketMiddleSectionView
            Spacer()
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
    }
}

struct MerketHomePageView_Previews: PreviewProvider {
    static var previews: some View {
        MerketHomePageView()
    }
}

extension MerketHomePageView {
    
    var marketHeaderView : some View {
        
        ZStack (alignment: .leading) {
            Color.pink.ignoresSafeArea()
            
            VStack (alignment: .leading, spacing: 2){
                HStack {
                    Button {
                        //actions here..//
                        isOpenWalletMenu.toggle()
                    } label: {
                        MenuButtonView(menuButtonView: "menucard")
                    }
                    .padding(.leading, 8)
                    .sheet(isPresented: $isOpenWalletMenu) {
                        //WalletView()
                    }
                    YemekPaketiHeaderView(yemekPeketiText: "MarketPaketi", yemekPaketiLogo: "signature", yemekPaketiFont: .title3, yemekPaketiSecondFont: Font.headline.weight(.bold), spacing: 6, yemekPaketiColor: .pink)
                    Spacer()
                    HStack (spacing: 15){
                        Button {
                            isOpenLikeMenu.toggle()
                        } label: {
                            
                            if vm.saveLikes.isEmpty {
                                MenuButtonView(menuButtonView: "heart")
                            } else {
                                MenuButtonView(menuButtonView: "heart")
                                    .overlay(
                                        Circle()
                                            .fill(Color.yellow)
                                            .frame(width: 14,height: 14)
                                            .overlay(
                                                Text("\(vm.saveLikes.count)")
                                                    .font(.caption2)
                                                    .fontWeight(.semibold)
                                                    .foregroundColor(Color.black)
                                            )
                                       
                                        ,alignment: .topTrailing
                                    )
                            }
                        }
                        .sheet(isPresented: $isOpenLikeMenu) {
                            LikesView(vm: vm)
                        }
                        Button {
                            //actions here..//
                            isOpenPayMenu.toggle()
                        } label: {
                            if selectedMenu.count >= 1 {
                                MenuButtonView(menuButtonView: "cart.fill.badge.plus")
                                    .overlay(
                                        Circle()
                                            .fill(Color.yellow)
                                            .frame(width: 14,height: 14)
                                            .overlay(
                                                Text("1")
                                                    .font(.caption2)
                                                    .fontWeight(.semibold)
                                                    .foregroundColor(Color.black)
                                            )
                                       
                                        ,alignment: .topTrailing
                                    )
                            }else {
                                MenuButtonView(menuButtonView: "cart.fill.badge.plus")
                            }
                        }
                    }
                }
                .padding(.horizontal)
                
                HStack (alignment: .bottom, spacing: 25){
                    Image("logo")
                        .resizable()
                        .frame(width: 50, height: 50)
                        .clipShape(Circle())
                    CustomTextAreaView(searchPlaceHolder: "Ara", searchIcon: "magnifyingglass.circle")
                }
                .padding(.bottom, 0)
                .padding(.top)
                .padding(.horizontal)
            }
            .sheet(isPresented: $isOpenPayMenu) {
                PayPageView()
            }
        }
        .frame(height: 150)
        .padding(.bottom,-13) //? dogru mu

    }
    
    var marketCaroselView : some View {
        VStack {
            TabView {
                ForEach(marketCaroselData) { item in
                    CaroselView(cardData: item)
                }
            }
            .tabViewStyle(.page(indexDisplayMode: .always))
            .indexViewStyle(.page(backgroundDisplayMode: .always))
            .padding(0)
        }
        .frame(height: 200)
        .padding(0)
        //.offset(x: 0, y: -13)
    }
    
    var marketMiddleSectionView : some View {
        ScrollView {
            ScrollView (.horizontal, showsIndicators: false) {
                HStack (spacing: 16){
                    ForEach(cardMarketArray) { item in
                        NavigationLink {
                            FoodDetailView(cardData: item, vm: vm)
                                .navigationBarBackButtonHidden(true)
                                .navigationBarHidden(true)
                        } label: {
                            CardView(cardData: item)
                        }
                        .simultaneousGesture(
                            TapGesture().onEnded{
//                                        let indexes = cardArray.enumerated().filter{ $0.element.title == item.title }.map{ $0.offset }
//                                        print(indexes)
                                indexArray = cardMarketArray.firstIndex(where: {$0.title == item.title}) ?? 0
                                print(indexArray)
                        })
                    }
                }
                .padding(.horizontal)
                
            }
            BestSellerTextView(bestSellerText: "Best Seller Menu")
            Divider()
                .frame(height: 4)
                .background(.gray).opacity(0.2)
            LazyVGrid(columns: [GridItem(.adaptive(minimum: 160))]) {
                ForEach(cardMarketArray.reversed()) { item in
                    SmallCardView(cardData: item)
                }
            }
            .padding(.horizontal)
            BestSellerTextView(bestSellerText: "Kitchen & Categories")
            Divider()
            
            ScrollView (.horizontal, showsIndicators: false) {
                HStack (spacing: 16) {
                    ForEach(cardArray) { item in
                        CatogoriesView(cardData: item, Width: 100, Height: 100, IconSize: 45, fontSize: .title2, colorType: isMarket ? .pink : .orange)
                    }
                }
                .padding()
                Divider()
            }
            FooterView()
            Divider()
        }
    }
}
