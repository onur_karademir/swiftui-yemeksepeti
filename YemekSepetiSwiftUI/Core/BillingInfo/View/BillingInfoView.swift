//
//  BillingInfoView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 7.09.2022.
//

import SwiftUI

struct BillingInfoView: View {
    var body: some View {
        VStack {
            CustomHeaderSet(headerSmallText: "Profile Info", headerLogo: "logo", isButtonVisible: true)
                .padding(.horizontal)
            
            //RegisterCardView(regCardTitle: "Registered Card", regCardImg: "card1", regCardNoTitle: "Card No:")
            
            ScrollView {
                
                BillingAdressView(billingTitle: "Billing address :", billingIcon: "highlighter", billingSecondIcon: "pencil.and.outline", billingChangeText: "Change", billingAdress: "Simply dummy text of the printing and typesetting industry.")
                
                BestSellerTextView(bestSellerText: "Billing list.")
                Divider()
                
                ForEach(billingArray, id:\.id) { item in
                    BillingUserView(billingUserTitle: item.key, billingUserAdress: item.value)
                }
                
            }
            Spacer()
            
        }
        .navigationTitle("")
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
    }
}

struct BillingInfoView_Previews: PreviewProvider {
    static var previews: some View {
        BillingInfoView()
    }
}
