//
//  LikesView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 11.09.2022.
//

import SwiftUI

struct LikesView: View {
    
    // Observed Object //
    @ObservedObject var vm : CoreDataUserViewModel
    // app storage //
    @AppStorage("is_market") var isMarket : Bool = false
    var body: some View {
        VStack {
            PayPageHeaderView(closeButtonIcon: "xmark", headerLogo: "logo")
            
            ScrollView {
                
                if vm.saveLikes.isEmpty {
                    VStack {
                        AuthenticationHeaderView(titleText: "Hello.", subtitleText: "Your Favourite list is empty.", image: "logo", bgColor: .pink)
                        BestSellerTextView(bestSellerText: "Kitchen & Categories")
                        Divider()                        
                        ScrollView (.horizontal, showsIndicators: false) {
                            HStack (spacing: 16) {
                                ForEach(cardArray) { item in
                                    CatogoriesView(cardData: item, Width: 70, Height: 70, IconSize: 30, fontSize: .subheadline, colorType: isMarket ? .pink : .orange)
                                }
                            }
                            .padding()
                        }
                        BestSellerTextView(bestSellerText: "Last Seen")
                        Divider()
                        ScrollView (.horizontal, showsIndicators: false) {
                            HStack (spacing: 16) {
                                ForEach(cardArray) { item in
                                    MiniCardView(image: item.image, title: item.title, price: item.price, discount: item.discount, icon: item.icon)
                                }
                            }
                            .padding(.vertical)
                        }


                    }
                } else {
                    cardRowView
                }
            }
        }
    }
}

extension LikesView {
    
    var cardRowView : some View {
        VStack {
            
            YemekPaketiHeaderView(yemekPeketiText: "YemekPaketi Favourite List", yemekPaketiLogo: "signature", yemekPaketiFont: .title3, yemekPaketiSecondFont: Font.subheadline.weight(.bold), spacing: 10, yemekPaketiColor: .pink)
            
            LikeCountHeaderView(likeHeader: "Number of favorites: ", likeCount: "\(vm.saveLikes.count)")
            
            ForEach(vm.saveLikes.reversed(),id:\.id) { item in
                VStack {
                    VStack {
                        HStack (alignment: .top) {
                            Image(item.image)
                                .resizable()
                                .scaledToFill()
                                .frame(width: 120, height: 120)
                                .clipped()
                                .cornerRadius(20)
                                .overlay(
                                    Image(systemName: "heart.fill")
                                        .offset(x: 15, y: -8)
                                        .font(.title)
                                        .foregroundColor(.pink)
                                    ,alignment: .topTrailing
                                )
                            VStack (alignment: .leading, spacing: 7) {
                                Text("Restourant name:")
                                    .font(.subheadline)
                                    .foregroundColor(.gray)
                                Text(item.rest)
                                    .font(.title3)
                                    .fontWeight(.semibold)
                                Text("Food name:")
                                    .font(.subheadline)
                                    .foregroundColor(.gray)
                                Text(item.food)
                                    .font(.subheadline)
                            }
                            .padding(.vertical)
                            .padding(.leading)
                            Spacer()
                            VStack (spacing: 20){
                                
                                Button {
                                    // delete manuel //
                                    //vm.saveLikes.remove(at:vm.saveLikes.firstIndex(of: item)!)
                                    
                                    // delete vire model call fuction //
                                    vm.deleteLikes(item: item)
                                } label: {
                                    Image(systemName: "trash")
                                        .font(.caption)
                                        .foregroundColor(.white)
                                        .padding(5)
                                        .background(.red)
                                        .cornerRadius(5)
                                }

                            }
                            .padding(.vertical)
                        }
                    }
                    .frame(minWidth: 0,maxWidth: .infinity,minHeight: 0,maxHeight: .infinity,alignment: .leading)
                    .padding(6)
                    .background(Color(.systemGray6))
                    .cornerRadius(20)
                }
                .padding(.horizontal)

            }
        }

    }
}

struct LikesView_Previews: PreviewProvider {
    static var previews: some View {
        LikesView(vm: CoreDataUserViewModel.init())
    }
}
