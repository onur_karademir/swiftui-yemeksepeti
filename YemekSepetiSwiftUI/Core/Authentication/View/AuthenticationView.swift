//
//  AuthenticationView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 8.09.2022.
//

import SwiftUI

struct AuthenticationView: View {
    // app storage //
    @AppStorage("user_core_data_check") var isUserCoreData : Bool = false
    // alert
    @State var alertTitle : String = ""
    @State var showAlert : Bool = false
    
    @State var email = ""
    @State var password = ""
    // stateObject
    @StateObject var vm = CoreDataUserViewModel()
    var body: some View {
        VStack {
            //header
            AuthenticationHeaderView(titleText: "Hello.", subtitleText: "Welcome Back", image: "logo", bgColor: .pink)
            //textfields
            VStack (spacing: 40){
                TextFieldRowView(placeholder: "Email", radius: 15, bindingText: $email)
                    .keyboardType(.emailAddress)
                    .textInputAutocapitalization(.never)
                SecureField( "Password", text: $password)
                    .padding()
                    .background(Color.white)
                    .cornerRadius(15)
            }
            .padding(.horizontal, 15)
            .padding(.top, 44)
            //sign in button
            Button {
                //signIn
                checkLogin()
            } label: {
                AuthButtonView(buttonString: "Sign In")
            }
            .shadow(color: .gray, radius: 1, x:0 , y: 0)

            Spacer()
            
            NavigationLink {
                RegisterView()
                    .navigationBarHidden(true)
            } label: {
                AuthFooterButtonView(textOne: "Don't have a accont?", textTwo: "Sign Up")
            }
            .foregroundColor(.blue)
            .padding()
            .padding(.bottom)
            .alert(isPresented: $showAlert) {
                return Alert(title: Text(alertTitle))
            }
        }
        .background(Color(.systemGray5))
        .ignoresSafeArea()
    }
    func checkLogin() {
        if email.count >= 3 && password.count >= 3 {
            guard email.contains("@") else {
                // show e-mail fail alert
                alertTitle = "Are you sure it's an email address? 🧐"
                showAlert.toggle()
                return
            }
            vm.checkUser(user_email: email, user_password: password)
            if isUserCoreData {
                alertTitle = "User not found! 🥸"
                showAlert.toggle()
            }
        } else {
            // show fail alert
            alertTitle = "All fields must be at least 3 characters long! 🧐"
            showAlert.toggle()
        }
    }
}

struct AuthenticationView_Previews: PreviewProvider {
    static var previews: some View {
        AuthenticationView()
    }
}
