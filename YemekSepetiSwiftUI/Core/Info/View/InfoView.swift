//
//  InfoView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 10.09.2022.
//

import SwiftUI

struct InfoView: View {
    @Binding var IsUpIconImage : Bool
    var body: some View {
        VStack {
            HStack (alignment: .center ,spacing: 0) {
                Text("Project Info")
                    .font(.title2)
                    .fontWeight(.semibold)
                Image(systemName: IsUpIconImage ? "chevron.down" : "chevron.up")
                    .font(.system(size: 35, weight: .semibold))
                    .foregroundColor(IsUpIconImage ? .orange : .pink)
            }
            .padding(.top, 9)
            VStack (alignment: .center, spacing: 25) {
                ScrollView {
                    AuthenticationHeaderView(titleText: "Welcome,", subtitleText: "Info Section.", image: "logo", bgColor: .pink)
                    InfoCardView(infoCardImg: "person", infoCardTitle: "Project Owner", infoCardSubtitle: "ONUR KARADEMIR")
                    InfoSmallCardView(infoSmallCardText: "This application is made for educational purposes. it has no commercial purpose. Thanks.")
                }
                
               
                Spacer()
            }
            .ignoresSafeArea()
        }
        .navigationTitle("")
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        .background(Color(.systemGray5))
    }
}

struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        InfoView(IsUpIconImage: .constant(false))
    }
}
