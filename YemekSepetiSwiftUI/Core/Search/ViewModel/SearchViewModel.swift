//
//  SearchViewModel.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 22.09.2022.
//

import Foundation

struct SearchModel : Identifiable, Equatable {
    let id = UUID()
    let text : String
    let image : String
}

class SearchViewModel : ObservableObject {
    @Published var searchViewModel : [SearchModel] = []
    init() {
        getData()
    }
    
    func getData() {
        let item1 = SearchModel(text: "item1", image: "food1")
        let item2 = SearchModel(text: "item2", image: "food2")
        let item3 = SearchModel(text: "item3", image: "food3")
        let item4 = SearchModel(text: "item4", image: "food4")
        let item5 = SearchModel(text: "item5", image: "food5")
        let item6 = SearchModel(text: "item6", image: "food6")
        let item7 = SearchModel(text: "item7", image: "market1")
        let item8 = SearchModel(text: "item8", image: "market2")
        
        
        searchViewModel.append(item1)
        searchViewModel.append(item2)
        searchViewModel.append(item3)
        searchViewModel.append(item4)
        searchViewModel.append(item5)
        searchViewModel.append(item6)
        searchViewModel.append(item7)
        searchViewModel.append(item8)
    }
}
