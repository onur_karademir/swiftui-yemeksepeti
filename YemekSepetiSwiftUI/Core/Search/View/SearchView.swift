//
//  SearchView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 22.09.2022.
//

import SwiftUI

struct SearchView: View {
    @Environment(\.presentationMode) var presentationMode
    @StateObject var vm : SearchViewModel = SearchViewModel()
    @State var searchText : String = ""
    @State var text : String = ""
    let myColum : [GridItem] = [
        GridItem(.flexible(), spacing: nil, alignment: nil),
        GridItem(.flexible(), spacing: nil, alignment: nil),
        GridItem(.flexible(), spacing: nil, alignment: nil),
        GridItem(.flexible(), spacing: nil, alignment: nil)
        
    ]
    var body: some View {
        VStack (alignment:.leading, spacing: 20) {
            HStack {
                Spacer()
                Button {
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    SearchCencelButtonView(cancelButtonText: "Cancel", cancelButtonColor: .pink)
                }

            }
            
            SearchTitleView(titleString: "Search Items")
            
            LazyVGrid (
                columns: myColum,
                      alignment: .center,
                      spacing: 10,
                      pinnedViews: [.sectionHeaders],
                      content: {
                          ForEach(vm.searchViewModel,id:\.id) { item in
                              HStack (spacing: 6) {
                                  Image(item.image)
                                      .resizable()
                                      .frame(width: 20, height: 20, alignment: .center)
                                      .clipShape(Circle())
                                  Text(item.text)
                                      .foregroundColor(.white)
                                      .font(.footnote)
                                      //.frame(width: 40)
                              }
                              .frame(width: 70)
                              .padding(7)
                              .background(.pink)
                              .clipShape(Capsule())
                              .overlay(
                                Image(systemName: "xmark")
                                    .foregroundColor(.white)
                                    .font(.system(size: 13, weight: .bold))
                                    .onTapGesture {
                                        vm.searchViewModel.remove(at:vm.searchViewModel.firstIndex(of: item)!)
                                    }
                                ,alignment:.topTrailing
                          )
                        }
                    })
            .frame(height: 150)

            TextFieldRowView(placeholder: "Ara", radius: 30, bindingText: $searchText)
            Button {
                text = searchText
                searchText = ""
            } label: {
                Text("Search")
                    .foregroundColor(.white)
                    .frame(maxWidth: .infinity)
                    .padding()
                    .background(.pink)
                    .clipShape(Capsule())
            }
            Text(text)
                .font(.largeTitle)
            Spacer()
        }
        .padding()
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}
