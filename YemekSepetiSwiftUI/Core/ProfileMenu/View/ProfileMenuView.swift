//
//  ProfileMenuView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 30.08.2022.
//

import SwiftUI

struct ProfileMenuView: View {
    //app storage
    @AppStorage("index_number") var indexNumber : Int = 0
    @AppStorage("index_data_array") var indexArray : Int = 0
    @AppStorage("selected_menu") var selectedMenu : String = ""
    var body: some View {
        Text(cardArray[indexArray].title)
        Text(cardArray[indexArray].restName)
        Text(selectedMenu)
    }
}

struct ProfileMenuView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileMenuView()
    }
}
