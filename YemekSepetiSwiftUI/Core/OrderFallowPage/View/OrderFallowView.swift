//
//  OrderFallowView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 3.09.2022.
//

import SwiftUI

struct OrderFallowView: View {
    @ObservedObject var miniCardViewModel : MiniCardViewModel
    @AppStorage("index_ordered_array") var indexOrderArray : Int = 0
    @AppStorage("true_pay") var isTruePay : Bool =  false
    @AppStorage("is_market") var isMarket : Bool = false
    @State private var hasTimeElapsed = false
    private func delayText() {
         // Delay of 7.5 seconds
         DispatchQueue.main.asyncAfter(deadline: .now() + 7.5) {
             self.hasTimeElapsed = true
             self.isTruePay = false
         }
     }
    private func delayView() {
         // Delay of 7.5 seconds
         DispatchQueue.main.asyncAfter(deadline: .now() + 7.5) {
             self.hasTimeElapsed = false
             self.isTruePay = false
         }
     }
    var body: some View {
        VStack {
            
            CustomHeaderSet(headerSmallText: "Order Fallowing", headerLogo: "logo", isButtonVisible: false)
            .padding(.horizontal)
            
            YemekPaketiHeaderView(yemekPeketiText: isMarket ? "MarketPaketi" : "YemekPaketi", yemekPaketiLogo: "signature", yemekPaketiFont: .title, yemekPaketiSecondFont: Font.title2.weight(.bold), spacing: 20, yemekPaketiColor: .pink)
            
            if isTruePay {
                VStack {
                    HStack (alignment: .center, spacing: 20) {
                        Image(isMarket ? cardMarketArray[indexOrderArray].image : cardArray[indexOrderArray].image)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 100, height: 88)
                            .clipped()
                            .cornerRadius(10)
                            .padding(.trailing, 20)
                        VStack (alignment: .leading, spacing: 10) {
                            HStack {
                                Text(isMarket ? cardMarketArray[indexOrderArray].restName : cardArray[indexOrderArray].restName)
                                
                                Text(isMarket ? cardMarketArray[indexOrderArray].title : cardArray[indexOrderArray].title)
                            }
                            .font(.headline)
                            .foregroundColor(.black)
                            Label("25 mn.", systemImage: "clock.badge.exclamationmark")
                                .font(.subheadline)
                                .foregroundColor(.secondary)
                        }
                    }
                }
                .onAppear(perform: delayText)
                Divider()

            } else if !isTruePay && hasTimeElapsed{
                PayNotifView(isPayText: "Ordered Succes", isPayImg: "figure.wave", paySubTitle: "")
                    .onAppear(perform: delayView)
            }else {
                PayPageEmptyView(emPtyText: "Delivery List Empty", emptyImg: "figure.wave", emptySubTitle: "")
            }
            
            ScrollView (showsIndicators: false) {
                BestSellerTextView(bestSellerText: "Last Seen")
                Divider()
                ScrollView (.horizontal, showsIndicators: false) {
                    HStack (spacing: 16) {
                        //eski hali kendi datasina bakiyor
                        //ForEach(miniCardViewModel.miniCardViewModel, id:\.id) {
                        //yeni hali genel dataya bakiyor
                        ForEach(cardArray) { item in
                            NavigationLink {
                                FoodDetailView(cardData: item)
                                    .navigationBarBackButtonHidden(true)
                                    .navigationBarHidden(true)
                            } label: {
                                MiniCardView(image: item.image, title: item.title, price: item.price, discount: item.discount, icon: item.icon)
                            }
                        }
                    }
                    .padding(.vertical)
                }
                BestSellerTextView(bestSellerText: "Kitchen & Categories")
                Divider()
                ScrollView (.horizontal, showsIndicators: false) {
                    HStack (spacing: 16) {
                        ForEach(isMarket ? cardMarketArray : cardArray) { item in
                            CatogoriesView(cardData: item, Width: 70, Height: 70, IconSize: 30, fontSize: .subheadline, colorType: isMarket ? .pink : .orange)
                        }
                    }
                    .padding()
                }

                //Spacer()
            }
        }
        .navigationTitle("")
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        .background(Color(.systemGray5))
    }
}

struct OrderFallowView_Previews: PreviewProvider {
    static var previews: some View {
        OrderFallowView(miniCardViewModel: MiniCardViewModel.init())
    }
}
