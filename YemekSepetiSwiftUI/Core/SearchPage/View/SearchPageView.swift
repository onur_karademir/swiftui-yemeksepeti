//
//  SearchPageView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 1.09.2022.
//

import SwiftUI

struct SearchPageView: View {
    var body: some View {
        VStack {
            Text("SearchPage")
        }
        .navigationTitle("")
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
    }
}

struct SearchPageView_Previews: PreviewProvider {
    static var previews: some View {
        SearchPageView()
    }
}
