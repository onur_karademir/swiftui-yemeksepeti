//
//  CustomTabView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 26.09.2022.
//

import SwiftUI

struct CustomTabView: View {
    @State var startinOffset : CGFloat = UIScreen.main.bounds.height * 0.78
    @State var currentOffset : CGFloat = 0
    @State var endingOffset : CGFloat = 0
    @State var IsUpIconImage : Bool = false
    @State var selected : Int = 0
    var body: some View {
        
        ZStack {
            VStack {
                
                HStack{Spacer()}
                
                HStack {
                    TopBarView(selected: $selected)
                }
                
                if selected == 0 {
                    VStack (alignment: .leading) {
                        SearchTitleView(titleString: "Top Visit Stars")
                        ScrollView (showsIndicators: false) {
                            ForEach(cardArray) { item in
                                TopBarListView(cardData: item)
                            }
                        }
                    }
                    .padding(.horizontal, 7)
                    
                } else {
                    VStack (alignment: .leading) {
                        SearchTitleView(titleString: "Top Comment")
                        CommentRowView()
                    }
                    .padding(.horizontal, 7)
                }
                Spacer()
            }
            .navigationTitle("")
            .navigationBarBackButtonHidden(true)
            .navigationBarHidden(true)
            .background(Color(.systemGray6))
            InfoView(IsUpIconImage: $IsUpIconImage)
                .offset(y: startinOffset)
                .offset(y: currentOffset)
                .offset(y: endingOffset)
                .gesture(
                    DragGesture()
                        .onChanged{ value in
                            withAnimation (.spring()) {
                                currentOffset = value.translation.height
                            }
                        }
                        .onEnded{ value in
                            withAnimation (.spring()) {
                                
                                if currentOffset < -150 {
                                    endingOffset = -startinOffset
                                    currentOffset = 0
                                    IsUpIconImage = true
                                } else if endingOffset != 0 && currentOffset > 150{
                                    endingOffset = 0
                                    IsUpIconImage = false
                                }
                                currentOffset = 0
                                
                            }
                        }
                )
        }
        
    }
}

struct CustomTabView_Previews: PreviewProvider {
    static var previews: some View {
        CustomTabView()
    }
}
