//
//  RegisterView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 8.09.2022.
//

import SwiftUI
// import core data for buying list ? //
import CoreData

struct Likes: Identifiable, Equatable {
    var id = UUID()
    var food: String
    var rest: String
    var image: String
}

class CoreDataUserViewModel : ObservableObject  {
    // empty array for likes //
    @Published var saveLikes : [Likes] = []
    func addLikes(like: Likes) {
           saveLikes.append(like)
        }
    
    func deleteLikes(item: Likes) {
        saveLikes.remove(at:saveLikes.firstIndex(of: item)!)
    }
    // container core data //
    let container : NSPersistentContainer
    // my entity //
    @Published var saveEntities : [UserEntity] = []
    
    // app storage
    @AppStorage("signed_in") var currentUserSignedIn : Bool = false
    @AppStorage("user_core_data_check") var isUserCoreData : Bool = false
    @AppStorage("user_check_sign_in") var checkSignEmailCont : Bool = false
    @AppStorage("user_name") var currentUserName: String?
    @AppStorage("pm_point") var currentUserPm : Int = 0
    init () {
        container = NSPersistentContainer(name: "UserContainer")//<=dosya ismi olmai mutlaka !!! //
        
        container.loadPersistentStores { description, error in
            if let error = error {
                print("error loading core data ! \(error)")
            }
        }
        fatchItems()
    }
        
    func fatchItems () {
        let request = NSFetchRequest<UserEntity>(entityName: "UserEntity")
        do {
            saveEntities = try container.viewContext.fetch(request)
        }catch let error {
            print("error \(error)")
        }
    }
    
    func addPaketMoney(paket_money: Int, current_user: String) {
        let request = NSFetchRequest<UserEntity>(entityName: "UserEntity")
        do {
            let result = try container.viewContext.fetch(request)
            let checkEmail = result.filter({ (user) -> Bool in
                return user.userEmail == current_user
            })
            if !checkEmail.isEmpty {
                checkEmail[0].userPaketMoney = Int16(paket_money)
                saveData()
                print("record money name and point -->> \(checkEmail[0].userEmail ?? "user$") & \(checkEmail[0].userPaketMoney)")
            }
        }catch let error {
            print("error \(error)")
        }
    }
    
    func addUserData (user_name: String, user_email: String, user_password: String) {
        let request = NSFetchRequest<UserEntity>(entityName: "UserEntity")
        do {
            // user email filter ?? //
            let result = try container.viewContext.fetch(request)
            let checkSignEmail = result.filter({ (user) -> Bool in
                return user.userEmail == user_email
            })
            // end //
            
            // if user email already bu taken trigger alert //
            // not save data //
            if !checkSignEmail.isEmpty {
                checkSignEmailCont = true
            } else {
                checkSignEmailCont = false //<--sonradan ekledigim kisim//
                // this part core data record user //
                let newItem = UserEntity(context: container.viewContext)
                newItem.userName = user_name
                newItem.userEmail = user_email
                newItem.userPassword = user_password
                newItem.userPaketMoney = 0
                saveData()
                // end //
            }
        }catch let error {
            print("error \(error)")
        }
        
    }
    
    
    func checkUser (user_email: String, user_password: String) {
        
        let request = NSFetchRequest<UserEntity>(entityName: "UserEntity")
        do {
            let result = try container.viewContext.fetch(request)
            let checkEmail = result.filter({ (user) -> Bool in
                return user.userEmail == user_email
            })
            
            let checkPassword = result.filter({ (user) -> Bool in
                return user.userPassword == user_password
            })
            
            if !checkEmail.isEmpty && !checkPassword.isEmpty{
                fatchItems()
                currentUserSignedIn = true
                currentUserName = user_email
                isUserCoreData = false
                currentUserPm = Int(checkEmail[0].userPaketMoney)
            }else {
                fatchItems()
                currentUserSignedIn = false
                isUserCoreData = true
            }
            print("checkEmail -->> \(checkEmail)")
            print("checkPassword -->> \(checkPassword)")
            
            
            for items in result {
                if items.userEmail == user_email && items.userPassword == user_password{
                    //currentUserSignedIn = true
                    print("dogru")
                    print("dogru password-)\(user_password)")
                    print("dogru email-)\(user_email)")
                    print("dogru user name-)\(user_email)")
                }else {
                    //currentUserSignedIn = false
                    print("yanlis")
                }


                print("user name -- \(items.userName ?? "")")
                print("email -- \(items.userEmail ?? "")")
                print("password -- \(items.userPassword ?? "")")
            }

        }catch let error {
            print("error \(error)")
        }

    }
    
    func delataAll() {
        // delete all core data entitys //
        let context = container.viewContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserEntity")
                let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try context.execute(deleteRequest)
            try context.save()
        }catch let error {
            print("error \(error)")
        }
    }
    
    func deleteUser() {
        let request = NSFetchRequest<UserEntity>(entityName: "UserEntity")
        do {
            let result = try container.viewContext.fetch(request)
            for item in result {
                if item.userEmail == currentUserName {
                    container.viewContext.delete(item)
                    currentUserSignedIn = false
                    saveData()
                }
            }
        }catch let error {
            print("error \(error)")
        }
        
    }
    
    func saveData () {
        do {
            try container.viewContext.save()
            fatchItems()
        }catch let error{
            print("error \(error)")
        }
    }
}

struct RegisterView: View {
    
    @State var email = ""
    @State var password = ""
    @State var username = ""
    // alert// stateObject
    @StateObject var vm = CoreDataUserViewModel()
    @State var alertRegisterTitle : String = ""
    @State var showAlertRegister : Bool = false
    
    // app storage
    @AppStorage("signed_in") var currentUserSignedIn : Bool = false
    @AppStorage("user_check_sign_in") var checkSignEmailCont : Bool = false
    var body: some View {
        VStack {
            //hearder
            AuthenticationHeaderView(titleText: "Get Started.", subtitleText: "Create Accont.", image: "logo", bgColor: .pink)
            
            //textfields
            
            VStack (spacing: 25){
                
                TextFieldRowView(placeholder: "Email", radius: 15, bindingText: $email)
                    .keyboardType(.emailAddress)
                    .textInputAutocapitalization(.never)
                
                TextFieldRowView(placeholder: "User Name", radius: 15, bindingText: $username)
                
                SecureField( "Password", text: $password)
                    .padding()
                    .background(Color.white)
                    .cornerRadius(15)
                
            }
            .padding(.horizontal, 15)
            .padding(.top, 44)
            
            Button {
                //actions
                signUp()
            } label: {
                AuthButtonView(buttonString: "Sign Up")
            }
            .shadow(color: .gray, radius: 1, x:0 , y: 0)
            
            Spacer()
            
            NavigationLink {
                AuthenticationView()
                    .navigationBarHidden(true)
            } label: {
                AuthFooterButtonView(textOne: "You have a accont?", textTwo: "Sign In")
            }
            .foregroundColor(.blue)
            .padding()
            .padding(.bottom)
            .alert(isPresented: $showAlertRegister) {
                return Alert(title: Text(alertRegisterTitle))
            }
        }
        .background(Color(.systemGray5))
        .ignoresSafeArea()
    }
    func signUp() {
        
        if email.count >= 3 && password.count >= 3 && username.count >= 3 {
            guard email.contains("@") else {
                //show e-mail fail alert
                alertRegisterTitle = "Are you sure it's an email address? 🧐"
                showAlertRegister.toggle()
                return
            }
            vm.addUserData(
                user_name: username,
                user_email: email,
                user_password: password
            )
            if checkSignEmailCont {
                alertRegisterTitle = "This user address has already been taken 😩"
                showAlertRegister.toggle()
            } else {
                //print data
                print("add save user info core data --->>> \(vm.saveEntities)")
                //show succes alert
                alertRegisterTitle = "Congratulations go to Sign in page down here 🥰 👇🏻 🎉"
                showAlertRegister.toggle()
            }
            
        } else {
            //show fail alert
            alertRegisterTitle = "All fields must be at least 3 characters long! 🧐"
            showAlertRegister.toggle()
        }
        
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView()
    }
}
