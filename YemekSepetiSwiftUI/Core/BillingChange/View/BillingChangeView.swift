//
//  BillingChangeView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 30.09.2022.
//

import SwiftUI

struct BillingChangeView: View {
    @State var rawString : String = ""
    @Binding var billingString : String
    var body: some View {
        VStack (alignment: .leading) {
            PayPageHeaderView(closeButtonIcon: "xmark", headerLogo: "logo")
            Spacer()
            ScrollView {
                VStack (alignment: .leading, spacing: 17) {
                    
                    Text("Change Adress : ")
                    
                    TextFieldRowView(placeholder: "Change Adress", radius: 15, bindingText: $rawString)
                    
                    Text("New Adress : ")
                    if !rawString.isEmpty {
                        Text(rawString)
                    } else {
                        Text("It can be viewed here before the address change.")
                            .font(.subheadline)
                            .fontWeight(.semibold)
                    }
                                        
                    Button {
                        billingString = rawString
                    } label: {
                        Text("Save Chage Adress")
                            .font(.subheadline)
                            .fontWeight(.semibold)
                            .padding(9)
                            .background(.pink)
                            .foregroundColor(.white)
                    }

                }
                
            }
            .padding()
            .background(Color(.systemGray6))
        }
    }
}

struct BillingChangeView_Previews: PreviewProvider {
    static var previews: some View {
        BillingChangeView(billingString: .constant(""))
    }
}
