//
//  ProfileDetailViewModel.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 7.09.2022.
//

import Foundation

struct ProfileDetailTypeModel : Identifiable {
    var id = UUID().uuidString
    var key : String
    var value : String
}

class ProfileDetailViewModel : ObservableObject {
    
    @Published var profileDetailViewModel : [ProfileDetailTypeModel] = []
    
    init() {
        getItems()
    }
    
    func getItems() {
        let item1 = ProfileDetailTypeModel(key: "Name", value: "Add Name")
        let item2 = ProfileDetailTypeModel(key: "Desc", value: "Add here")
        let item3 = ProfileDetailTypeModel(key: "Location", value: "Loction add here")
        let item4 = ProfileDetailTypeModel(key: "Web Site", value: "Web site add here")
        
        profileDetailViewModel.append(item1)
        profileDetailViewModel.append(item2)
        profileDetailViewModel.append(item3)
        profileDetailViewModel.append(item4)
        
    }
}
