//
//  ProfileInfoView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 5.09.2022.
//

import SwiftUI

struct ProfileInfoView: View {
    // state object //
    @StateObject var profileStateObj : ProfileDetailViewModel = ProfileDetailViewModel()
    
    var body: some View {
        VStack {
            CustomHeaderSet(headerSmallText: "Profile Info", headerLogo: "logo", isButtonVisible: true)
            .padding(.horizontal)
            ProfileHeaderView(createListHeaderImg: "camera")
            
            ScrollView {
                
                VStack (spacing: 29){
                    ForEach(profileStateObj.profileDetailViewModel, id:\.id) { item in
                        ProfileTextAreaView(createListKey: item.key, createListValue: item.value)
                    }
                }
                .padding(.top, 25)
                
            }
            Spacer()
            
        }
        .navigationTitle("")
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
    }
}

struct ProfileInfoView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileInfoView()
    }
}
