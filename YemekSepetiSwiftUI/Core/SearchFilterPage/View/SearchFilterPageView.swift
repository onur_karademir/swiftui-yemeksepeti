//
//  SearchFilterPageView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 24.09.2022.
//

import SwiftUI

struct SearchFilterPageView: View {
    // environment //
    @Environment(\.presentationMode) var presentationMode
    // state object //
    @StateObject var vm : CoreDataUserViewModel = CoreDataUserViewModel()
    // state varible //
    @State var searchText = ""
    // app storage //
    @AppStorage("index_data_array") var indexArray : Int = 0
    @AppStorage("is_market") var isMarket : Bool = false
    // column //
    let myColum : [GridItem] = [
        GridItem(.flexible(), spacing: nil, alignment: nil),
        GridItem(.flexible(), spacing: nil, alignment: nil),
        GridItem(.flexible(), spacing: nil, alignment: nil),
        GridItem(.flexible(), spacing: nil, alignment: nil)
        
    ]

    var body: some View {
        NavigationView {
            VStack {
               VStack (alignment: .leading, spacing: 20) {
                HStack {
                    Spacer()
                    Button {
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        SearchCencelButtonView(cancelButtonText: "Cancel", cancelButtonColor: .pink)
                    }

                }
                SearchTitleView(titleString: "Search Items")
                
                LazyVGrid (
                    columns: myColum,
                          alignment: .center,
                          spacing: 10,
                          pinnedViews: [.sectionHeaders],
                          content: {
                              ForEach(isMarket ? cardMarketArray : cardArray,id:\.id) { item in
                                  HStack (spacing: 6) {
                                      Image(item.image)
                                          .resizable()
                                          .frame(width: 20, height: 20, alignment: .center)
                                          .clipShape(Circle())
                                      Text(item.title)
                                          .foregroundColor(.white)
                                          .font(.footnote)
                                          .onTapGesture {
                                              print("mini card value ==>> \(item.title)")
                                              searchText = item.title
                                          }
                                  }
                                  .frame(width: 70)
                                  .padding(7)
                                  .background(.pink)
                                  .clipShape(Capsule())
                                  .overlay(
                                    Image(systemName: "hand.point.up.fill")
                                        .offset(x: -5, y: -5)
                                        .foregroundColor(.black)
                                        .font(.system(size: 13, weight: .bold))
                                        .onTapGesture {
                                            
                                        }
                                    ,alignment:.topTrailing
                              )
                              }
                        })

                TextFieldRowView(placeholder: "Search", radius: 30, bindingText: $searchText)
                       .onChange(of: searchText) {
                            print("Search letters --> \($0)") // You can do anything due to the change here.
                                // self.autocomplete($0) // like this
                        }
            }
            .padding()
                
                VStack (alignment:.leading, spacing: 0) {
                    /*if !cardArray.contains(where: {$0.title == searchText}) {
                      Text("Not Found")
                    }*/
                    if searchText.isEmpty {
                        // ?? bu kismi dusuncem //
                    } else {
                     ScrollView (showsIndicators: false) {
                         VStack (spacing: 0) {
                             ForEach((isMarket ? cardMarketArray : cardArray).filter({ "\($0)".contains(searchText)})) { item in
                                NavigationLink {
                                    FoodDetailView(cardData: item, vm: vm)
                                        .navigationBarBackButtonHidden(true)
                                        .navigationBarHidden(true)
                                } label: {
                                    SearchItemView(cardData: item)
                                }
                                .simultaneousGesture (
                                    TapGesture().onEnded{
                                        if isMarket {
                                            indexArray = cardMarketArray.firstIndex(where: {$0.title == item.title}) ?? 0
                                        } else {
                                            indexArray = cardArray.firstIndex(where: {$0.title == item.title}) ?? 0
                                        }
                                        print("selected SEARCH item index number --> \(indexArray)")
                                })

                             }
                         }
                      }
                    }
                    
                }
                //.animation(.easeInOut(duration: 0.6))
                //.background(.white)
                .navigationBarBackButtonHidden(true)
                .navigationBarHidden(true)
                //.navigationTitle("Search")
                //.navigationBarTitleDisplayMode(.inline)
//                .navigationBarItems(trailing:
//                    Button {
//                        presentationMode.wrappedValue.dismiss()
//                    } label: {
//                    SearchCencelButtonView(cancelButtonText: "Cancel", cancelButtonColor: .pink)
//                    }
//                )
                Spacer()
            }
            .background(Color(.systemGray2))
            .modifier(BackgroundImage())
            
        }
        
        //.ssearchable(text: $searchText, placement: .navigationBarDrawer(displayMode: .always))
    }
}

struct SearchFilterPageView_Previews: PreviewProvider {
    static var previews: some View {
        SearchFilterPageView()
    }
}

struct BackgroundImage: ViewModifier {
    func body(content: Content) -> some View {
        content
            .opacity(0.9)
            .background(Image("ss")
            .resizable()
            .scaledToFill())
            //.ignoresSafeArea()
    }
}
