//
//  YemekSepetiSwiftUIApp.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 30.08.2022.
//

import SwiftUI

@main
struct YemekSepetiSwiftUIApp: App {
    @AppStorage("signed_in") var currentUserSignedIn : Bool = false
    var body: some Scene {
        WindowGroup {
            NavigationView {
                if currentUserSignedIn {
                    ContentView()
                    //HomePageView()
                }else {
                    AuthenticationView()
                }
            }
        }
    }
}
