//
//  MarketContentView.swift
//  YemekSepetiSwiftUI
//
//  Created by Onur on 16.09.2022.
//

import SwiftUI

struct MarketContentView: View {
    var body: some View {
        ZStack (alignment: .top){
            MarketTabView()
        }
    }
}

struct MarketContentView_Previews: PreviewProvider {
    static var previews: some View {
        MarketContentView()
    }
}
